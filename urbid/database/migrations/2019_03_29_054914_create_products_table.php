<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->string('product_code');
            $table->string('product_name');
            $table->string('made_in');
            $table->longText('product_description');
            $table->float('product_cost');
            $table->float('product_com_profit');
            $table->integer('product_quantity');
            $table->boolean('is_offer');
            $table->String('subcategory_code');
            $table->foreign('subcategory_code')->references('subcategory_code')->on('subcategories');
            $table->char('product_status',2);
            $table->string('product_image');
            $table->primary('product_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
