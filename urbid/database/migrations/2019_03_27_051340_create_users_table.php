<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('client_name');
            $table->string('client_lastName');
            $table->char('client_role',2);
            $table->string('client_phone');
            $table->string('picture');
            $table->string('facebook_id')->unique()->nullable();
            $table->string('player_id');
            $table->string('phone_code');
            $table->String('warehouse_code')->nullable();
            $table->foreign('warehouse_code')->references('warehouse_code')->on('warehouses');
            $table->integer('points');
            $table->boolean('phone_validated');
            $table->boolean('allow_notifications');
            $table->char('validation_method');
            $table->string('password');
            $table->char('client_status',2);
            $table->timestamps();
            $table->string('remember_token', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
