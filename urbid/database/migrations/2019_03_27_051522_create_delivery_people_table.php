<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('delivery_person_email');
            $table->string('delivery_person_name');
            $table->string('delivery_person_lastName');
            $table->string('picture');
            $table->char('delivery_person_identificationType',2);
            $table->string('delivery_person_identificationNumber');
            $table->char('delivery_person_role',2);
            $table->string('delivery_person_phone');
            $table->string('player_id');
            $table->string('phone_code');
            $table->boolean('phone_validated');
            $table->char('validation_method');
            $table->string('password');
            $table->char('delivery_person_status',2);
            $table->integer('total_viajes');
            $table->timestamps();
            $table->string('remember_token', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_people');
    }
}
