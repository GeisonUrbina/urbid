<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_code')->unique();
            $table->string('project_name');
            $table->longText('project_description');
            $table->integer('project_cost');
            $table->integer('project_collected_amount');
            $table->char('project_status',2);
            $table->string('project_image');
            $table->String('warehouse_code');
            $table->foreign('warehouse_code')->references('warehouse_code')->on('warehouses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
