<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->string('warehouse_code')->unique();
            $table->string('warehouse_country');
            $table->string('province_code');
            $table->string('canton_code');
            $table->string('district_code');
            $table->char('warehouse_status',2);
            $table->string('warehouse_name');
            $table->integer('warehouse_workers');
            $table->primary('warehouse_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouses');
    }
}
