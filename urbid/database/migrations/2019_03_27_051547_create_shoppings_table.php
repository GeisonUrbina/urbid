<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoppings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->float('montoTotal');
            $table->float('montoPagar');
            $table->char('forma_pago',2);
            $table->char('status',2);
            $table->longText('carrito');
            $table->unsignedInteger('id_usuario');
            $table->foreign('id_usuario')->references('id')->on('users');
            $table->unsignedInteger('id_repartidor');
            $table->foreign('id_repartidor')->references('id')->on('delivery_people');
            $table->string('longitud');
            $table->string('latitud');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoppings');
    }
}
