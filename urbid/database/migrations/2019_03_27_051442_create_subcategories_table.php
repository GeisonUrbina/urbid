<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategories', function (Blueprint $table) {
            $table->string('subcategory_code');
            $table->String('category_code');
            $table->foreign('category_code')->references('category_code')->on('categories');
            $table->string('subcategory_name');
            $table->char('subcategory_status',2);
            $table->string('subcategory_image');
            $table->primary('subcategory_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategories');
    }
}
