<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityOrganizationChartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_organization_charts', function (Blueprint $table) {
            $table->increments('id');
            $table->String('warehouse_code')->nullable();
            $table->foreign('warehouse_code')->references('warehouse_code')->on('warehouses');
            $table->string('presidente');
            $table->string('p_imagen')->nullable();;
            $table->string('p_identificacion')->nullable();
            $table->string('vicepresidente');
            $table->string('v_imagen')->nullable();
            $table->string('v_identificacion')->nullable();
            $table->string('tesorero');
            $table->string('t_imagen')->nullable();
            $table->string('t_identificacion')->nullable();
            $table->string('secretario');
            $table->string('s_imagen')->nullable();
            $table->string('s_identificacion')->nullable();
            $table->string('vocal_1');
            $table->string('v1_imagen')->nullable();
            $table->string('v1_identificacion')->nullable();
            $table->string('vocal_2');
            $table->string('v2_imagen')->nullable();
            $table->string('v2_identificacion')->nullable();
            $table->string('vocal_3');
            $table->string('v3_imagen')->nullable();
            $table->string('v3_identificacion')->nullable();
            $table->string('fiscal');
            $table->string('f_imagen')->nullable();
            $table->string('f_identificacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_organization_charts');
    }
}
