@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Crear categoría</h1>
    <hr/>
    @if(session()->has('success'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
	    	@endforeach
	  	  </div>
		</div>
	@endif
	<form method="post" action="new-category" enctype="multipart/form-data">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input name="category_code" type="text" class="form-control" value="{{ old('category_code') }}" id="codigo_categoria" placeholder="Ingrese el código de la categoría">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="category_image" type="file" accept="image/*">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input name="category_name" type="text" class="form-control" value="{{ old('category_name') }}" id="nombre_categoria" placeholder="Ingrese el nombre de la categoría">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<select name="category_status" value="{{ old('category_status') }}" class="form-control">
					<option value="A">Activar</option>
					<option value="B">Bloquear</option>
				</select>
            </div>
        </div>
		<button type="submit" class="btn bg-primary text-white">Agregar categoría</button>
	</form>

@endsection