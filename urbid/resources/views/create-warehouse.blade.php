@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Crear almacen</h1>
    <hr/>
    @if(session()->has('success'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
		    @endforeach
		  </div>
		</div>
	@endif
	<form method="post" action="new-warehouse">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="warehouse_code" class="form-control" id="codigo_bodega" placeholder="Ingrese el código de la bodega">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<select name="warehouse_country" class="form-control">
				  <option vlaue="CR">Costa Rica</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<select name="province_code" class="form-control" onchange="getCantons(this)">
				  <option value="select">Seleccione</option>
				  @foreach($provinces as $province)
					<option value="{{$province->province_code}}">{{$province->province_name}}</option>
				  @endforeach
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<select name="canton_code" id="canton_list" onchange="getDistricts(this)" class="form-control">
				  <option value="select">Seleccione</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<select name="district_code" id="district_list" class="form-control">
				  <option value="select">Seleccione</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="warehouse_name" class="form-control" id="nombre_bodega" placeholder="Ingrese el nombre de la bodega">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="number" name="warehouse_workers" min="1" class="form-control" id="cantidad_colaboradores" placeholder="Ingrese la cantidad de colaboradores">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<select name="warehouse_status" class="form-control">
				  <option value="A">Activar</option>
				  <option value="B">Bloquear</option>
				</select>
            </div>
        </div>
        <button type="submit" class="btn bg-primary text-white">Agregar bodega</button>
	</form>

		<script>
	function getCantons(selectedProvince){
        var CSRF_TOKEN = document.getElementsByName('_token')[0].value;
        var province = selectedProvince.value;
        $.ajax({
            url: '/search-cantons',
            type: 'post',
            data: {_token: CSRF_TOKEN,province_code : province},
            success: function (response) {

            	for(i=0;i<response.length;i++){
            	$("#canton_list").append('<option value="'+response[i]['canton_code']+'">'+response[i]['canton_name']+'</option>');
            	}
               
            },
            error: function(xhr, ajaxOptions, thrownError){
            	if(xhr.status == "404"){
            		$('#canton_list').empty().append('<option value="select">Seleccione</option>');
            		$('#district_list').empty().append('<option value="select">Seleccione</option>');
            	}
            }
        });
	}

	function getDistricts(selectedCanton){
		var CSRF_TOKEN = document.getElementsByName('_token')[0].value;
        var canton = selectedCanton.value;
        $.ajax({
            url: '/search-districts',
            type: 'post',
            data: {_token: CSRF_TOKEN,canton_code : canton},
            success: function (response) {

            	for(i=0;i<response.length;i++){
            	$("#district_list").append('<option value="'+response[i]['district_code']+'">'+response[i]['district_name']+'</option>');
            	}
               
            },
            error: function(xhr, ajaxOptions, thrownError){
            	if(xhr.status == "404"){
            		$('#district_list').empty().append('<option value="select">Seleccione</option>');
            	}
            }
        });
	}

</script>
@endsection