@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Crear producto</h1>
    <hr/>
    @if(session()->has('success'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
	    	@endforeach
	  	  </div>
		</div>
	@endif
	<form method="post" action="new-product" enctype="multipart/form-data">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
			    <input type="text" name="product_code" class="form-control" id="codigo_producto" placeholder="Ingrese el código del producto">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="product_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="product_name" class="form-control" id="nombre_producto" placeholder="Ingrese el nombre del producto">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-2 mb-0 mb-sm-0">
               <input type="number" name="product_cost" class="form-control" placeholder="Costo" min="1">
            </div>
            <div class="col-sm-2 mb-0 mb-sm-0">
               <input type="number" name="product_com_profit" class="form-control" placeholder="Aporte" min="1">
            </div>
            <div class="col-sm-2 mb-0 mb-sm-0">
               <input type="number" name="product_quantity" class="form-control" id="cantidad_producto" placeholder="Cantidad stock" min="1">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-6 mb-sm-0">
				<select name="category_code" onchange="getSubcategories(this)" class="form-control">
				    <option value="select">Seleccione categoría</option>
					@foreach ($categories as $category)
						<option value="{{$category->category_code}}">{{$category->category_name}}</option>
				    @endforeach
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-6 mb-sm-0">
				<select name="subcategory_code" id="subcategory_list" class="form-control">
					<option value="select">Seleccione subcategoría</option>
				</select>
            </div>
        </div>
		<div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="made_in" class="form-control" id="origen_producto" placeholder="Ingrese el pais de origen">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<textarea class="form-control" name="product_description" id="descripcion_producto" placeholder="Ingrese la descripción del producto" style="resize: none;"></textarea>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-6 mb-sm-0">
				<select name="product_status" class="form-control">
					<option value="A">Activar</option>
					<option value="B">Bloquear</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-12 mb-12 mb-sm-0">
				<button type="submit" class="btn bg-gradient-success btn-user text-white">Agregar producto</button>
			</div> 
        </div>
	</form>
	<script>
		function getSubcategories(selectedCategory){
			var CSRF_TOKEN = document.getElementsByName('_token')[0].value;
	        var category = selectedCategory.value;
	        $.ajax({
	            url: '/search-subcategories',
	            type: 'post',
	            data: {_token: CSRF_TOKEN,category_code : category},
	            success: function (response) {
	            	for(i=0;i<response.length;i++){
	            	$("#subcategory_list").append('<option value="'+response[i]['subcategory_code']+'">'+response[i]['subcategory_name']+'</option>');
	            	}
	               
	            },
	            error: function(xhr, ajaxOptions, thrownError){
	            	if(xhr.status == "404"){
	            		$('#subcategory_list').empty().append('<option value="select">Seleccione</option>');
	            	}
	            }
	        });
		}
	</script>
@endsection