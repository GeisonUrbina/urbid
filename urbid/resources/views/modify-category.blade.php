@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Modificar categoría</h1>
    <hr/>
    @if(session()->has('success'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
	    	@endforeach
	  	  </div>
		</div>
	@endif
	<form class="navbar-form" method="get" action="search-category">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-3 mb-3 mb-sm-0">
				<input type="text" name="search_category_code" class="form-control" value="" placeholder="Buscar categoría">
            </div>
            <button type="submit" class="btn bg-primary text-white">Buscar</button>
        </div>
	</form>
		@if(!empty($category_data))
		    <form method="post" action="update-category" enctype="multipart/form-data">
		    	@csrf
				<div class="form-group row">
		        	<div class="col-sm-6 mb-6 mb-sm-0">
				    	<input name="category_code" type="text" class="form-control" id="codigo_producto" value="{{$category_data->category_code}}" placeholder="Ingrese el código del producto" readonly>
		            </div>
		            <div class="col-sm-3 mb-3 mb-sm-0">
					    <input name="category_image" type="file" accept="image/*" id="exampleInputFile">
		            </div>
		        </div>

		        <div class="form-group row">
		        	<div class="col-sm-6 mb-6 mb-sm-0">
				    	<input name="category_name" type="text" class="form-control" value="{{$category_data->category_name}} "id="nombre_producto" placeholder="Ingrese el nombre del producto">
		            </div>
		        </div>

		        <div class="form-group row">
		        	<div class="col-sm-6 mb-6 mb-sm-0">
						<select name="category_status" class="form-control">
					      @if($category_data->category_status == 'A')
						  <option value="A" selected>Activar</option>
						  <option value="B">Bloquear</option>
						  @endif
						  @if($category_data->category_status == 'B')
						  <option value="A">Activar</option>
						  <option value="B" selected>Bloquear</option>
						  @endif
						</select>
		            </div>
		        </div>

		        <div class="form-group row">
		            <div class="col-sm-3 mb-3 mb-sm-0">
		            	<img  alt="200x200" src="../../img/category_images/{{$category_data->category_image}}" data-holder-rendered="true" style="width: 412px; height: 100px;">
		            </div>
		        </div>
				<button type="submit" class="btn bg-primary text-white">Modificar categoría</button>
			</form>
		@endif
@endsection