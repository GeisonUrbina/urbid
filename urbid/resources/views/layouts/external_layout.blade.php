<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <link rel="icon" href="/img/logo.jpg">
      <title>URBID</title>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="materialize/css/materialize.css" media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
      <script src="js/jquery-2.1.4.js"></script>
    </head>
    <body>
      <nav>
        <div class="nav-wrapper">
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a href="/">Inicio</a></li>
            <li><a href="/#proveedor">Proveedores</a></li>
            <li><a href="/#ubicacion">Ubicación</a></li>
          </ul>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li>TEL: (506) 2275-0553</li>
            <li><a href="/preguntas-frecuentes">FAQ</a></li>
          </ul>
        </div>    
      </nav>

    @yield('content')
    <footer class="page-footer cyan darken-3">
          <div class="container">
            <div class="row">
              <div class="col l3 s6">
                <h5 class="white-text">Ubicación</h5>
                <p class="grey-text text-lighten-4">San Juan de Dios, Desamparados, San José, Costa Rica.</p>
                <p class="grey-text text-lighten-4">Teléfono: (506) 2275-0553</p>
              </div>
              <div class="col l3 s6">
                <h5 class="white-text">Ayuda</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="/preguntas-frecuentes">Pregúntas frecuentes</a></li>
                  <li><a class="grey-text text-lighten-3" href="/terminos-condiciones">Términos y condiciones</a></li>
                </ul>
              </div>
              <div class="col l3 s6">
                <h5 class="white-text">Redes sociales</h5>
                <ul>
                  <a class="grey-text text-lighten-4 " href="https://www.facebook.com/urbidcr/"><i style="font-size: 2em;" class="fab fa-facebook-square"></i></a>
                  <a class="grey-text text-lighten-4 " href="https://www.instagram.com/urbid.app/"><i style="font-size: 2em;" class="fab fa-instagram"></i></a>
                </ul>
              </div>
              <div class="col l3 s6">
                <h5 class="white-text">Descargar</h5>
                <a href='https://play.google.com/store/apps/details?id=com.urbidcr.shopping'><img alt='Disponible en Google Play' height="70" width="200" src='https://play.google.com/intl/en_us/badges/images/generic/es-419_badge_web_generic.png'/></a>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            <div class="row">
              <div class="col s6 offset-s5 full-display">
                <p>© 2020 Copyright URBIDCR</p>
              </div>
            </div>
            </div>
          </div>
        </footer>
      <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
    </body>
  </html>