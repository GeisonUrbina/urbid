<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <link rel="icon" href="/img/logo.jpg">
      <title>URBID</title>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="materialize/css/materialize.css" media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
      <script src="js/jquery-2.1.4.js"></script>
    </head>
    <body>
      <nav>
        <div class="nav-wrapper">
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a href="/">Inicio</a></li>
            <li><a href="/#proveedor">Proveedores</a></li>
            <li><a href="/#ubicacion">Ubicaciones</a></li>
          </ul>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li>TEL: (506) 2275-0553</li>
            <li><a href="/preguntas-frecuentes">FAQ</a></li>
          </ul>
        </div>    
      </nav>

    @yield('content')
    </body>
  </html>