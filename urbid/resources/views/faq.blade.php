@extends('layouts.external_layout')

@section('content')

      <div class="container">
          <div class="row">
            <div class="col s12"><h4 class="cyan-text text-darken-2">Pregúntas frecuentes</h4><hr></div>
            
            <div class="col s10">
              <h6 class="cyan-text text-darken-2">¿Qué es la aplicación URBID?</h6>
              <p >
                URBID ofrece la venta de productos de abarrotes, cuidado personal y productos para el hogar.
              </p>

              <h6 class="cyan-text text-darken-2">¿Cuales son las formas de pago?</h6>
              <p >
                Los medios de pagos disponibles son: Efectivo, Sinpe móvil
              </p>

              <h6 class="cyan-text text-darken-2">¿Qué es Sinpe movil?</h6>
              <p >
                SINPE Móvil se dirige al segmento de pagos al detalle (de bajo monto), para que los usuarios del Sistema Financiero Nacional puedan realizar transferencias electrónicas de dinero a cuentas vinculadas a números de teléfono móviles, desde cualquier canal de banca electrónica (Banca SMS, Banca Web Móvil, Banca App, Banca en Línea o Red de Cajeros Automáticos). <a href="https://www.bccr.fi.cr/seccion-sistema-de-pagos/servicios/pagos-en-tiempo-real/sinpe-móvil" target="_blank">Leer más</a><br><br>
                Fuente: BCCR 
              </p>

              <h6 class="cyan-text text-darken-2">¿Es seguro utilizar los servicio de URBID?</h6>
              <p >
                URBID es un medio seguro para realizar sus compras, no es necesario ingresar datos de tu tarjeta de débito o crédito. Realiza tus envíos hasta la puerta de su casa.
              </p>

              <h6 class="cyan-text text-darken-2">¿Para qué dispositivo está disponible la aplicación?</h6>
              <p >
                La aplicación está disponible para los celulares Android con un modelo mayor a 4.4
              </p>

              <h6 class="cyan-text text-darken-2">¿De qué forma URBID apoya a las comunidades?</h6>
              <p >
                URBID apoya a las comunidades organizadas mediante donaciones por cada producto vendido en la aplicación.
              </p>

              <h6 class="cyan-text text-darken-2">¿Qué productos ofrece URBID?</h6>
              <p >
                URBID vende productos como abarrotes, cuidado personal, productos para el hogar, entre otros
              </p>


              <h6 class="cyan-text text-darken-2">¿Cómo puedo actualizar mis datos personales?</h6>
              <p >
                Para actualizar tus datos personales, debes dirigirte a la sección de configuración y presionar el icono de lapiz en el dato que deseas modificar.
              </p>

              <h6 class="cyan-text text-darken-2">¿Qué son los proyectos comunitarios?</h6>
              <p >
                Los proyectos comunitarios son todas aquellas obras que las comundidades organizadas acuerdan desarrollar para mejorar la calidad de vida de sus residentes.
              </p>

              <h6 class="cyan-text text-darken-2">¿Cómo puedo afiliarme a un proyecto comunitario?</h6>
              <p >
                Para afiliarse a un proyecto comunitario debes dirigirte en la aplicación en la sección <b>Proyectos comunitarios</b>, se desplegará una lista de los proyectos comunitarios creados.
                Presionar el botón Ver detalle, luego en la pagina de detalle presionar el botón Afiliarse
              </p>

              <h6 class="cyan-text text-darken-2">¿Cómo puedo desafiliarme a un proyecto comunitario?</h6>
              <p >
                Para desafiliarse a un proyecto comunitario debes dirigirte en la aplicación en la sección <b>Proyectos comunitarios</b>, se desplegará una lista de los proyectos comunitarios creados.
                Presionar el botón Ver detalle, luego en la pagina de detalle presionar el botón Desafiliarse
              </p>

              <h6 class="cyan-text text-darken-2">¿Cómo puedo colaborar a un proyecto comunitario?</h6>
              <p >
                Al comprar productos por medio de la aplicación URBID, un monto de cada producto se destinará al proyecto comunitario en el que estés afiliado.
              </p>

              <h6 class="cyan-text text-darken-2">No se muestra el mapa en la pantalla de ubicación</h6>
              <p >
                Debes comprobar que tengas acceso a la red de internet, luego darle permisos a la aplicación de tu ubicación. Si has realizado los pasos anterior y aún no es posible
                visualizar el mapa, reinicie el dispositivo.
              </p>

            </div>
          </div>
      </div>
@endsection
