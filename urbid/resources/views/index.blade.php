@extends('layouts.external_layout')

@section('content')
      <div class="background-image-index">
        <div class="container">
          <div class="row">
            <div class="col s6 full-display">
              <h3 class="white-text">¿Quienes somos?</h3>
              <p class="white-text">URBID es una empresa dedicada a la venta de abarrotes y productos para el hogar, por medio de la app <a href="https://play.google.com/store/apps/details?id=com.spotify.music&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" style="color:#ffab00 !important"><b>URBIDCR</b></a> con entrega a domicilio.</p>
              <a href='https://play.google.com/store/apps/details?id=com.urbidcr.shopping'><img alt='Disponible en Google Play' height="70" width="200" src='https://play.google.com/intl/en_us/badges/images/generic/es-419_badge_web_generic.png'/></a>
            </div>
            <div class="col s6 push-s2 hide-on-med-and-down"><img src="img/phone.png" style="border-radius:25px;"></div>
          </div>
        </div>
      </div>
      <div class="container">
          <div class="row">
            <div class="col s12"><h4 class="cyan-text text-darken-2 center-align">Beneficios</h4><hr class="cyan darken-3"/></div>
            <div class="col s4">
              <div class="center promo promo-example">
                <i class="fas fa-hand-holding-usd" style="font-size: 5em;"></i>
                <p >
                  URBID provee de un aporte económico por cada producto vendido a las comunidades organizadas afiliadas. Permitiendo el desarrollo de proyectos.
                </p>
              </div>
            </div>
            <div class="col s4">
              <div class="center promo promo-example">
                <i class="fas fa-recycle" style="font-size: 5em;"></i>
                <p >
                  Proyectos comunitarios en mejoras del medio ambiente. Recolección de basura, limpieza de zonas verdes
                </p>
              </div>
            </div>
            <div class="col s4">
              <div class="center promo promo-example">
                <i class="fas fa-bicycle" style="font-size: 5em;"></i>
                <p >
                  No tendrás que trasladarte para obtener los productos que deseas, ya que te lo llevamos hasta la puerta de tu casa.
                </p>
              </div>
            </div>
          </div>
      </div>
      <div class="main-container" id="ubicacion">
        <div class="container">
            <div class="row">
              <div class="col s6 hide-on-med-and-down"><img height="300" width="500" class="max-size" src="img/mapa.png"></div>
              <div class="col s6"><h4 class="cyan-text text-darken-2">Ubicación</h4></div>
              <div class="col s6 full-display">
                <p class="text-darken-2 center-aligns">
                  URBID se encuentra ubicado en San Juan de Dios, Desamparados, San José, Costa Rica. Específicamente en
                  la urbanización Itaipú. Mediante convenios con el comité de desarrollo de la comunidad, realiza
                  proyectos, actividades de bien social y ambiental.
                </p>
              </div>
            </div>
        </div>
      </div>
      <div class="main-container" id="proveedor">
        <div class="container">
            <div class="row">
              <div class="col s6 hide-on-med-and-down"><img height="300" width="500" class="max-size" src="img/proveedor.png"></div>
              <div class="col s6"><h4 class="cyan-text text-darken-2">Proveedores</h4></div>
              <div class="col s6 full-display">
                <p class=" text-darken-2 center-aligns">
                  Si eres un proveedor de productos como abarrotes, cuidado personal, productos para el hogar, entre otros y los quieres dar a conocer a una gran cantidad de clientes, no dudes en contactarnos al número teléfonico (506) 2275-0553
                </p>
              </div>
            </div>
        </div>
      </div>
      <div class="container">
          <div class="row">
            <div class="col s12"><h4 class="cyan-text text-darken-2 center-align">Pregúntas Frecuentes</h4><hr class="cyan darken-3"/></div>
            <div class="col s6">
              <div class="center promo promo-example"><h6 class="cyan-text text-darken-2">¿De qué forma URBID apoya a las comunidades?</h6>
                <p>URBID apoya a las comunidades organizadas mediante donaciones por cada producto vendido en la aplicación.</p>
              </div>
            </div>
            <div class="col s6">
              <div class="center promo promo-example">
                <h6 class="cyan-text text-darken-2">¿Qué productos ofrece URBID?</h6>
                <p>URBID vende productos como abarrotes, cuidado personal, productos para el hogar, entre otros.</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s6">
              <div class="center promo promo-example">
                <h6 class="cyan-text text-darken-2">¿Qué son los proyectos comunitarios?</h6>
                <p>Los proyectos comunitarios son todas aquellas obras que las comundidades organizadas acuerdan desarrollar para mejorar la calidad de vida de sus residentes.</p>
              </div>
            </div>
            <div class="col s6">
              <div class="center promo promo-example">
                <h6 class="cyan-text text-darken-2">¿Cómo puedo afiliarme a un proyecto comunitario?</h6>
                <p>
                  Para afiliarse a un proyecto comunitario debes dirigirte en la aplicación en la sección <b>Proyectos comunitarios</b>, se desplegará una lista de los proyectos comunitarios creados.
                  Presionar el botón Ver detalle, luego en la pagina de detalle presionar el botón Afiliarse
                </p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s6">
              <div class="center promo promo-example">
                <h6 class="cyan-text text-darken-2">¿Cómo puedo colaborar a un proyecto comunitario?</h6>
                <p>Al comprar productos por medio de la aplicación URBID, un monto de cada producto se destinará al proyecto comunitario en el que estés afiliado.</p>
              </div>
            </div>
              <div class="col s6">
                <div class="center promo promo-example">
                  <h6 class="cyan-text text-darken-2">¿Cuales son las formas de pago?</h6>
                  <p>Los medios de pagos disponibles son: Efectivo, SINPE Móvil.</p>
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col s6">
              <div class="center promo promo-example">
                <h6 class="cyan-text text-darken-2">¿Qué es SINPE Móvil?</h6>
                <p>
                  SINPE Móvil se dirige al segmento de pagos al detalle (de bajo monto), para que los usuarios del Sistema Financiero Nacional puedan realizar transferencias electrónicas de dinero a cuentas vinculadas a números de teléfono móviles, desde cualquier canal de banca electrónica (Banca SMS, Banca Web Móvil, Banca App, Banca en Línea o Red de Cajeros Automáticos). <a href="https://www.bccr.fi.cr/seccion-sistema-de-pagos/servicios/pagos-en-tiempo-real/sinpe-móvil" target="_blank">Leer más</a>
                  Fuente: BCCR 
                </p>
              </div>
            </div>
          </div>
      </div>
@endsection