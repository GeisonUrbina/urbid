@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Modificar almacen</h1>
    <hr/>
    @if(session()->has('success'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
		    @endforeach
		  </div>
		</div>
	@endif
	<form class="navbar-form" method="get" action="search-warehouse">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-3 mb-3 mb-sm-0">
				<input type="text" name="search_warehouse_code" class="form-control" value="" placeholder="Buscar bodega">
            </div>
            <button type="submit" class="btn bg-primary text-white">Buscar</button>
        </div>
	</form>
	@if(!empty($warehouse_data))
		<form method="post" action="update-warehouse">
			@csrf
			<div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text" name="warehouse_code" class="form-control" id="codigo_bodega" value="{{$warehouse_data->warehouse_code}}" placeholder="Ingrese el código de la bodega" readonly>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<select name="warehouse_country" class="form-control">
					  <option vlaue="CR">Costa Rica</option>
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<select name="province_code" class="form-control" onchange="getCantons(this)">
					  <option value="select">Seleccione</option>
					  @foreach($provinces as $province)
					  	@if($province->province_code == $warehouse_data->province_code)
							<option value="{{$province->province_code}}" selected>{{$province->province_name}}</option>
					  	@else
							<option value="{{$province->province_code}}">{{$province->province_name}}</option>
					  	@endif
					  @endforeach
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<select name="canton_code" id="canton_list" onchange="getDistricts(this)" class="form-control">
					  <option value="select">Seleccione</option>
					  @foreach($cantons as $canton)
					  	@if($canton->canton_code == $warehouse_data->canton_code)
							<option value="{{$canton->canton_code}}" selected>{{$canton->canton_name}}</option>
					  	@else
							<option value="{{$canton->canton_code}}">{{$canton->canton_name}}</option>
					  	@endif
					  @endforeach
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<select name="district_code" id="district_list" class="form-control">
					  <option value="select">Seleccione</option>
					  @foreach($districts as $district)
					  	@if($district->district_code == $warehouse_data->district_code)
							<option value="{{$district->district_code}}" selected>{{$district->district_name}}</option>
					  	@else
							<option value="{{$district->district_code}}">{{$district->district_name}}</option>
					  	@endif
					  @endforeach
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text" name="warehouse_name" class="form-control" value="{{$warehouse_data->warehouse_name}}" id="nombre_bodega" placeholder="Ingrese el nombre de la bodega">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="number" name="warehouse_workers" min="1" class="form-control" value="{{$warehouse_data->warehouse_workers}}" id="cantidad_colaboradores" placeholder="Ingrese la cantidad de colaboradores">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<select name="warehouse_status" class="form-control">
					  @if($warehouse_data->warehouse_status == 'A')
					  <option value="A" selected>Activar</option>
					  <option value="B">Bloquear</option>
					  @endif
					  @if($warehouse_data->warehouse_status == 'B')
					  <option value="A">Activar</option>
					  <option value="B" selected>Bloquear</option>
					  @endif
					</select>
	            </div>
	        </div>
			<button type="submit" class="btn bg-primary text-white">Agregar bodega</button>
		</form>
	@endif
@endsection