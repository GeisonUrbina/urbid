@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Detalle de compra</h1>
    <hr/>
    <div class="card shadow mb-4">
		<div class="card-body" style="display: flex;">
			<div class="col-lg-6">
				<label>Código de compra: {{$listaCompra->codigo}}</label><br/>
			    <label>Fecha de creación: {{$listaCompra->created_at}}</label><br/>
			    <label>Estado: 
					@if($listaCompra->status == 'PC')
						Procesando compra
					@endif
					@if($listaCompra->status == 'PE') 
						Pendiente entrega
					@endif
				</label><br/>
			    <label>Forma Pago: 
					@if($listaCompra->forma_pago == 'E')
						Efectivo
					@endif
					@if($listaCompra->forma_pago == 'PE') 
						Pendiente entrega
					@endif
				</label><br/>
			    <label>Repartidor: {{$deliveryPerson->delivery_person_name}}</label><br/>
			</div>
			<div class="col-lg-3">
				<label>Monto compra: ₡{{number_format($listaCompra->montoTotal,2,'.',',')}}</label><br/>
				<label>Monto envío: ₡{{number_format($listaCompra->montoEnvio,2,'.',',')}}</label><br/>
				<label>Monto total: ₡{{number_format($listaCompra->montoTotal + $listaCompra->montoEnvio,2,'.',',')}}</label><br/>
			    <label>Monto pago: ₡{{number_format($listaCompra->montoPagar,2,'.',',')}}</label><br/>
			    <label>Monto vuelto: ₡ @if($listaCompra->forma_pago == 'E') 
								{{number_format($listaCompra->montoPagar - ($listaCompra->montoTotal + $listaCompra->montoEnvio),2,'.',',')}}
							@else
								{{number_format(0,2,'.',',')}}
	                      	@endif
			    <form method="post" action="modify-order-status">
					@csrf
					<input type="hidden" value="{{$listaCompra->codigo}}" name="orderCode" />
					<input type="hidden" value="CC" name="estado" />
					<button type="submit" class="btn bg-gradient-danger btn-user text-white">Cancelar compra</button>
				</form>
			</div>
		</div>
	</div>
    <div class="col-lg-6 mb-2">
		<div class="card text-white shadow">
	    	<div class="card-body">
				<h4 class="mb-4 text-gray-800">Lista de productos</h4>
				<hr/>
				@foreach($lista[0] as $producto)
					<div style="display:flex;">
						<div class="col-lg-3">
							<img src="http://www.urbidcr.com/img/product_images/{{$producto->product_image}}" style="height:80px;width:80px" alt="">
						</div>
						<div class="col-lg-9 text-gray-800">
							<label><b>{{$producto->product_name}}</b></label><br/>
							<label>Codigo: {{$producto->product_code}}</label><br/>
							<label>Cantidad: {{$producto->product_quantity}}</label><br/>
							<label>Costo: ₡{{$producto->product_cost}}</label>
						</div>
					</div>
					<hr/>
				@endforeach
			</div>
		</div>
	</div>


@endsection