@extends('layouts.internal_layout')

@section('content')
	<h1 class="h3 mb-4 text-gray-800">Pedidos solicitados</h1>
    <hr/>
         <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Código</th>
                      <th>Fecha</th>
                      <th>Monto Total</th>
                      <th>Monto Pago</th>
                      <th>Monto Vuelto</th>
                      <th>Repartidor</th>
                      <th>Forma de pago</th>
                      <th>Estado</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@for($i=0; $i < count($listaCompras);$i++)
						<tr>
	                      <td>{{$listaCompras[$i]->codigo}}</td>
	                      <td>{{$listaCompras[$i]->created_at}}</td>
	                      <td>{{number_format($listaCompras[$i]->montoTotal,2,'.',',')}}</td>
	                      <td>{{number_format($listaCompras[$i]->montoPagar,2,'.',',')}}</td>
	                      <td>{{number_format($listaCompras[$i]->montoPagar - $listaCompras[$i]->montoTotal,2,'.',',')}}</td>
	                      <td>
	                      	@foreach ($deliveryPeople as $person)
	                      		@if($person->id == $listaCompras[$i]->id_repartidor)
									{{$person->delivery_person_name}} {{$person->delivery_person_lastName}}
	                      		@endif
							@endforeach
	                      </td>
						  <td>Efectivo</td>
						  <td>
							@if($listaCompras[$i]->status == 'PC') Procesando compra @endif

							@if($listaCompras[$i]->status == 'PE') Pendiente entrega @endif

							@if($listaCompras[$i]->status == 'CC') Compra cancelada @endif
						  </td>
	                    </tr>
                  	@endfor
                  </tbody>
                </table>
                @if(count($listaCompras) == 0)
                <div class="col-lg-12 mb-2">
	                  <div class="card bg-danger text-white shadow">
	                    <div class="card-body">
	                      No hay pedidos solicitados
	                    </div>
	                  </div>
	             </div>
	            @endif
              </div>
            </div>
          </div>


@endsection