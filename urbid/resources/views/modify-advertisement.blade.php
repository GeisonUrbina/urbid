@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Modificar campaña</h1>
    <hr/>
    @if(session()->has('success') || session()->has('error'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
            <p>{{ session()->get('error') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
		    @endforeach
		  </div>
		</div>
	@endif
    <form class="navbar-form" method="get" action="search-advertisement">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-3 mb-3 mb-sm-0">
				<input type="text" name="search_advertisement_code" class="form-control" placeholder="Buscar campaña">
            </div>
            <button type="submit" class="btn bg-primary text-white">Buscar</button>
        </div>
	</form>
    @if(!empty($advertisement_data))
	<form method="post" action="update-advertisement" enctype="multipart/form-data">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="advertisement_code" class="form-control" id="codigo_campana" value="{{$advertisement_data->code}}" placeholder="Ingrese el código de la campaña" readonly>
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="advertisement_image" type="file" accept="image/*" id="imagen_campana">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="advertisement_name" class="form-control" id="nombre_campana" value="{{$advertisement_data->name}}" placeholder="Ingrese el nombre de la campaña">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="advertisement_priority" class="form-control">
					<option>Seleccione prioridad</option>
					@for($i = 1;$i < 11;$i++)
                        @if($advertisement_data->priority == $i)
						    <option value="{{$i}}" selected>{{$i}}</option>
                        @else
                            <option value="{{$i}}">{{$i}}</option>
                        @endif
					@endfor
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="advertisement_warehouse_code" class="form-control">
					<option value="">Todas las comunidades</option>
					@foreach($warehouses as $warehouse)
                        @if($advertisement_data->warehouse_code == $warehouse->warehouse_code)
						    <option value="{{$warehouse->warehouse_code}}" selected>{{$warehouse->warehouse_name}}</option>
                        @else
                            <option value="{{$warehouse->warehouse_code}}">{{$warehouse->warehouse_name}}</option>
                        @endif
					@endforeach
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="advertisement_category" class="form-control">
					    <option>Seleccione categoría</option>
					    <option value="PUBLICIDAD" @if($advertisement_data->category == 'PUBLICIDAD')selected @endif>Publicidad</option>
                        <option value="PROMOCION" @if($advertisement_data->category == 'PROMOCION')selected @endif>Promoción</option>
                        <option value="ACTIVIDAD_COMUNITARIA" @if($advertisement_data->category == 'ACTIVIDAD_COMUNITARIA')selected @endif>Actividad comunitaria</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="advertisement_location" class="form-control">
					    <option >Seleccione ubicación</option>
					    <option value="PRINCIPAL" @if($advertisement_data->position == 'PRINCIPAL')selected @endif>Principal</option>
                        <option value="ABAJO" @if($advertisement_data->position == 'ABAJO')selected @endif>Abajo</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="advertisement_status" class="form-control">
					<option value="A" @if($advertisement_data->status == 'A')selected @endif>Activar</option>
					<option value="B" @if($advertisement_data->status == 'B')selected @endif>Bloquear</option>
				</select>
            </div>
        </div>
		<button type="submit" class="btn bg-primary text-white">Modificar campaña</button>
	</form>
    @endif
@endsection