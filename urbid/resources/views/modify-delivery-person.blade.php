@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Modificar repartidor</h1>
    <hr/>
    @if(session()->has('success'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
	    	@endforeach
	  	  </div>
		</div>
	@endif
	<form class="navbar-form" method="get" action="search-delivery-person">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-3 mb-3 mb-sm-0">
				<select name="repartidor" id="" class="form-control">
					<option value="">Seleccionar</option>	
					@foreach ($deliveryPeople as $person)
						<option value="{{$person->id}}">{{$person->delivery_person_name}}</option>
					@endforeach	
				</select>
            </div>
            <button type="submit" class="btn bg-primary text-white">Buscar</button>
        </div>
	</form>
	@if(!empty($delivery_person_data))
		<form method="post" action="update-delivery-person">
		    @csrf
		    <input type="hidden" value="{{$delivery_person_data->id}}" name="id" />
		    <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text" name="nombre" class="form-control" id="nombre_repartidor" value="{{$delivery_person_data->delivery_person_name}}">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text" name="apellido" class="form-control" id="apellido_repartidor" value="{{$delivery_person_data->delivery_person_lastName}}">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<select name="tipo_identificacion" class="form-control">
				    	@if($delivery_person_data->delivery_person_identificationType == 'C')
						  <option value="C" selected>Cédula identidad</option>
					  	  <option value="P">Pasaporte</option>
					  	@endif
					  	@if($delivery_person_data->delivery_person_identificationType == 'P')
						  <option value="C">Cédula identidad</option>
					  	  <option value="P" selected>Pasaporte</option>
					  	@endif
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text" name="numero_identificacion" class="form-control" id="numero_identificacion" value="{{$delivery_person_data->delivery_person_identificationNumber}}">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text" name="correo_electronico" class="form-control" id="correo_electronico" value="{{$delivery_person_data->delivery_person_email}}">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text" name="numero_celular" class="form-control" id="numero_celular" value="{{$delivery_person_data->delivery_person_phone}}">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<select name="estado" class="form-control">
					  @if($delivery_person_data->delivery_person_status == 'A')
						  <option value="A" selected>Activar</option>
						  <option value="B">Bloquear</option>
					  @endif
					  @if($delivery_person_data->delivery_person_status == 'B')
						  <option value="A">Activar</option>
						  <option value="B" selected>Bloquear</option>
					  @endif
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text" name="contrasena" class="form-control" id="contrasena" placeholder="Ingrese la contraseña del repartidor">
	            </div>
	        </div>
	        <button type="submit" class="btn bg-primary text-white">Modificar repartidor</button>
		</form>
	@endif


@endsection