@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Modificar subcategoría</h1>
    <hr/>
    @if(session()->has('success'))
		<div class="panel green text-color-white">
			<div class="panel-body">
				<p>{{ session()->get('success') }}</p>
			</div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
			<div class="panel-body">
				@foreach ($errors->all() as $error)
					<p>{{ $error }}</p>
			    @endforeach
			</div>
		</div>
	@endif
	<form class="navbar-form" method="get" action="search-subcategory">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-3 mb-3 mb-sm-0">
				<input type="text" name="search_subcategory_code" class="form-control" placeholder="Buscar subcategoría">
            </div>
            <button type="submit" class="btn bg-primary text-white">Buscar</button>
        </div>
	</form>
	@if(!empty($subcategory_data))
		<form method="post" action="update-subcategory" enctype="multipart/form-data">
			@csrf
			<div class="form-group row">
	        	<div class="col-sm-6 mb-6 mb-sm-0">
					<input name="subcategory_code" type="text" class="form-control" id="codigo_subcategoria" value="{{$subcategory_data->subcategory_code}}"  readonly>
		        </div>
		        <div class="col-sm-3 mb-3 mb-sm-0">
					<input name="subcategory_image" type="file" accept="image/*" id="imagen_subcategoria">
		        </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-6 mb-sm-0">
					<input name="subcategory_name" type="text" class="form-control" id="nombre_subcategoria" value="{{$subcategory_data->subcategory_name}}" placeholder="Ingrese el nombre de la subcategoría">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-6 mb-sm-0">
					<select name="subcategory_category_index" class="form-control">
					  @foreach($categories as $category)
						  @if($category->category_code == $subcategory_data->category_code)
						  <option value="{{$category->category_code}}" selected>{{$category->category_name}}</option>
						  @else
						  <option value="{{$category->category_code}}">{{$category->category_name}}</option>
						  @endif
					  @endforeach
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-6 mb-sm-0">
					<select name="subcategory_status" class="form-control">
					  @if($subcategory_data->subcategory_status == 'A')
					  <option value="A" selected>Activar</option>
					  <option value="B">Bloquear</option>
					  @endif
					  @if($subcategory_data->subcategory_status == 'B')
					  <option value="A">Activar</option>
					  <option value="B" selected>Bloquear</option>
					  @endif
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-3 mb-3 mb-sm-0">
					<img class="rounded" alt="200x200" src="../../img/subcategory_images/{{$subcategory_data->subcategory_image}}" data-holder-rendered="true" style="width: 412px; height: 100px;">
	            </div>
	        </div>
	        <button type="submit" class="btn bg-primary text-white">Modificar Subcategoría</button>
		</form>
	@endif
@endsection