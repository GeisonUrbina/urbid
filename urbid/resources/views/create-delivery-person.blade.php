@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Crear repartidor</h1>
    <hr/>
    @if(session()->has('success'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
	    	@endforeach
	  	  </div>
		</div>
	@endif
	<form method="post" action="new-delivery-person">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="name" class="form-control" id="nombre_repartidor" placeholder="Ingrese el nombre del repartidor">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="lastName" class="form-control" id="apellido_repartidor" placeholder="Ingrese el apellido del repartidor">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<select name="identificationType" class="form-control">
				  <option value="C">Cédula identidad</option>
				  <option value="P">Pasaporte</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="identificationNumber" class="form-control" id="numero_identificacion" placeholder="Ingrese el número de identificación">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="email" class="form-control" id="correo_electronico" placeholder="Ingrese el correo electronico">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="phone" class="form-control" id="numero_celular" placeholder="Ingrese el número de celular">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<select name="state" class="form-control">
				  <option value="A">Activar</option>
				  <option value="B">Bloquear</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="password" class="form-control" id="contrasena" placeholder="Ingrese la contraseña del repartidor">
            </div>
        </div>
        <button type="submit" class="btn bg-primary text-white">Agregar repartidor</button>
	</form>

@endsection