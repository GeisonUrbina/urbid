@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Crear subcategoría</h1>
    <hr/>
    @if(session()->has('success'))
		<div class="panel green text-color-white">
			<div class="panel-body">
				<p>{{ session()->get('success') }}</p>
			</div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
			<div class="panel-body">
				@foreach ($errors->all() as $error)
					<p>{{ $error }}</p>
				@endforeach
			</div>
		</div>
	@endif
	<form method="post" action="new-subcategory" enctype="multipart/form-data">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="subcategory_code" class="form-control" id="codigo_subcategoria" placeholder="Ingrese el código de la subcategoría">            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input type="file" name="subcategory_image"  accept="image/*" id="subcategory_image">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="subcategory_name" class="form-control" id="nombre_subcategoria" placeholder="Ingrese el nombre de la subcategoría">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<select name="subcategory_category_index" class="form-control">
					@foreach ($categories as $category)
						<option value="{{$category->category_code}}">{{$category->category_name}}</option>
				    @endforeach
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<select name="subcategory_status" class="form-control">
					<option value="A">Activar</option>
					<option value="B">Bloquear</option>
				</select>
            </div>
        </div>
        <button type="submit" class="btn bg-primary text-white">Agregar subcategoría</button>
	</form>
@endsection