@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Modificar producto</h1>
    <hr/>
	@if(session()->has('success'))
	    <div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
			  	@foreach ($errors->all() as $error)
				    <p>{{ $error }}</p>
			    @endforeach
		  </div>
		</div>
	@endif
	<form class="navbar-form" method="get" action="search-product">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-3 mb-3 mb-sm-0">
				<input type="text" name="search_product_code" class="form-control" placeholder="Buscar producto">
            </div>
            <button type="submit" class="btn bg-primary text-white">Buscar</button>
        </div>
	</form>
	@if(!empty($product_data))
		<form method="post" action="update-product" enctype="multipart/form-data">
			@csrf
			<div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text"  name="product_code" class="form-control" id="codigo_producto" value="{{$product_data->product_code}}" placeholder="Ingrese el código del producto" readonly>
	            </div>
	            <div class="col-sm-3 mb-3 mb-sm-0">
					<input name="product_image" type="file" accept="image/*" id="imagen_producto">
	            </div>
	        </div>

	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text" name="product_name" value="{{$product_data->product_name}}"  class="form-control" id="nombre_producto" placeholder="Ingrese el nombre del producto">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="number" name="product_cost" pattern="[0-9]" value="{{$product_data->product_cost}}" class="form-control" id="costo_producto" placeholder="Ingrese el costo del producto" min="1">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
	        		<label>*Aporte del producto</label>
					<input type="number" name="product_com_profit" pattern="[0-9]" value="{{$product_data->product_com_profit}}" class="form-control" id="beneficio_producto" placeholder="Ingrese el aporte del producto" min="0">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="number" name="product_quantity" value="{{$product_data->product_quantity}}" class="form-control" id="cantidad_producto" placeholder="Ingrese la cantidad total del producto" min="0">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<select name="category_code" onchange="getSubcategories(this)" class="form-control">
				      <option value="select">Seleccione</option>
					  @foreach ($categories as $category)
							@if($category->category_code == $product_data->category_code)
								<option value="{{$category->category_code}}" selected>{{$category->category_name}}</option>
							@else
								<option value="{{$category->category_code}}">{{$category->category_name}}</option>
							@endif
				       @endforeach
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<select name="subcategory_code" id="subcategory_list" class="form-control">
				      <option value="select">Seleccione</option>
				      @foreach ($subcategories as $subcategory)
							@if($product_data->subcategory_code == $subcategory->subcategory_code)
								<option value="{{$subcategory->subcategory_code}}" selected>{{$subcategory->subcategory_name}}</option>
							@else
								<option value="{{$subcategory->subcategory_code}}">{{$subcategory->subcategory_name}}</option>
							@endif
					  @endforeach
					</select>
	            </div>
	        </div>
			<div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="made_in" class="form-control" id="origen_producto" value="{{$product_data->made_in}}" placeholder="Ingrese el pais de origen">
            </div>
        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<textarea class="form-control"  name="product_description" id="descripcion_producto" placeholder="Ingrese la descripción del producto" style="resize: none;">{{$product_data->product_description}}</textarea>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<select name="product_status" class="form-control">
					  @if($product_data->product_status == 'A')
					  <option value="A" selected>Activar</option>
					  <option value="B">Bloquear</option>
					  @endif
					  @if($product_data->product_status == 'B')
					  <option value="A">Activar</option>
					  <option value="B" selected>Bloquear</option>
					  @endif
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<img class="rounded" alt="200x200" src="../../img/product_images/{{$product_data->product_image}}" data-holder-rendered="true" style="width: 200px; height: 200px;">
	            </div>
	        </div>
	        <button type="submit" class="btn bg-primary text-white">Modificar producto</button>
		</form>
	@endif

<script>
	function getSubcategories(selectedCategory){
		var CSRF_TOKEN = document.getElementsByName('_token')[0].value;
        var category = selectedCategory.value;
        $.ajax({
            url: '/search-subcategories',
            type: 'post',
            data: {_token: CSRF_TOKEN,category_code : category},
            success: function (response) {
            	for(i=0;i<response.length;i++){
            	$("#subcategory_list").append('<option value="'+response[i]['subcategory_code']+'">'+response[i]['subcategory_name']+'</option>');
            	}
               
            },
            error: function(xhr, ajaxOptions, thrownError){
            	if(xhr.status == "404"){
            		$('#subcategory_list').empty().append('<option value="select">Seleccione</option>');
            	}
            }
        });
	}
</script>

@endsection