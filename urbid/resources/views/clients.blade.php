@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Lista de clientes</h1>
    <hr/>
     	<div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      	<th>#</th>
						<th>Nombre</th>
						<th>Correo electrónico</th>
						<th>Teléfono</th>
						<th>Estado</th>
						<th>Acción</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($clients as $client)
						<tr>
							<td>{{$loop->index}}</td>
							<td>{{$client->client_name}}</td>
							<td>{{$client->email}}</td>
							<td>{{$client->client_phone}}</td>
							<td>
							  @if($client->client_status == 'A')
							  Activo
							  @endif
							  @if($client->client_status == 'B')
							  Bloqueado
							  @endif
							</td>
							<td>
								@if($client->client_status == 'A')
								  <a role="button" href="update-client-status/{{$client->email}}/B">Activar/Bloquear</a>
								@endif
								@if($client->client_status == 'B')
								  <a role="button" href="update-client-status/{{$client->email}}/A">Activar/Bloquear</a>
								@endif
							</td>
						</tr>
					@endforeach
                  </tbody>
                </table>
                @if(empty($clients))
	                <div class="col-lg-12 mb-2">
		                  <div class="card bg-danger text-white shadow">
		                    <div class="card-body">
		                      No hay clientes agregados
		                    </div>
		                  </div>
		             </div>
	             @endif
              </div>
            </div>
          </div>
@endsection