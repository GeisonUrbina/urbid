@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Modificar proyecto comunitario</h1>
    <hr/>
    @if(session()->has('success'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
		    @endforeach
		  </div>
		</div>
	@endif
	<form class="navbar-form" method="get" action="search-project">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-3 mb-3 mb-sm-0">
				<input type="text" name="search_project_code" class="form-control" placeholder="Buscar proyecto">
            </div>
            <button type="submit" class="btn bg-primary text-white">Buscar</button>
        </div>
	</form>
	@if(!empty($project_data))
		<form method="post" action="update-project" enctype="multipart/form-data">
			@csrf
			<div class="form-group row">
	        	<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text"  name="project_code" class="form-control" id="codigo_proyecto" value="{{$project_data->project_code}}" placeholder="Ingrese el código del proyecto" readonly>
	            </div>
	            <div class="col-sm-3 mb-3 mb-sm-0">
					<input name="project_image" type="file" accept="image/*" id="imagen_proyecto">
	            </div>
	        </div>
			<div class="form-group row">
	        	<div class="col-sm-6">
					<input type="text" name="project_name" value="{{$project_data->project_name}}"  class="form-control" id="nombre_project" placeholder="Ingrese el nombre del proyecto">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6">
					<select name="warehouse_code" id="warehouse_list" class="form-control">
				      <option value="select">Seleccione</option>
				      @foreach ($warehouses as $warehouse)
							@if($project_data->warehouse_code == $warehouse->warehouse_code)
								<option value="{{$warehouse->warehouse_code}}" selected>{{$warehouse->warehouse_name}}</option>
							@else
								<option value="{{$warehouse->warehouse_code}}">{{$warehouse->warehouse_name}}</option>
							@endif
					  @endforeach
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6">
					<textarea class="form-control"  name="project_description" id="descripcion_proyecto" placeholder="Ingrese la descripción del proyecto" style="resize: none;">{{$project_data->project_description}}</textarea>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6">
					<input type="number" name="project_cost" value="{{$project_data->project_cost}}" class="form-control" id="costo_proyecto" placeholder="Ingrese el costo del proyecto" min="1">
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-6">
					<select name="project_status" class="form-control">
					  @if($project_data->project_status == 'A')
					  <option value="A" selected>Activar</option>
					  <option value="B">Bloquear</option>
					  @endif
					  @if($project_data->project_status == 'B')
					  <option value="A">Activar</option>
					  <option value="B" selected>Bloquear</option>
					  @endif
					</select>
	            </div>
	        </div>
	        <div class="form-group row">
	        	<div class="col-sm-3">
					<img class="rounded" alt="200x200" src="../../img/project_images/{{$project_data->project_image}}" data-holder-rendered="true" style="width: 200px; height: 200px;">
	            </div>
	        </div>
	        <button type="submit" class="btn bg-primary text-white">Modificar proyecto</button>
		</form>
	@endif

@endsection