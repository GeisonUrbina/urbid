@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Crear proyecto comunitario</h1>
    <hr/>
    @if(session()->has('success'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
		    @endforeach
		  </div>
		</div>
	@endif
	<form method="post" action="new-project" enctype="multipart/form-data">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="project_code" class="form-control" id="codigo_proyecto" placeholder="Ingrese el código del proyecto comunitario">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="project_image" type="file" accept="image/*" id="imagen_proyecto">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="project_name" class="form-control" id="nombre_proyecto" placeholder="Ingrese el nombre del proyecto comunitario">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="warehouse_code" class="form-control">
					<option value="select">Seleccione</option>
					@foreach($warehouses as $warehouse)
						<option value="{{$warehouse->warehouse_code}}">{{$warehouse->warehouse_name}}</option>
					@endforeach
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<textarea class="form-control" name="project_description" id="descripcion_proyecto" placeholder="Ingrese la descripción del proyecto" style="resize: none;"></textarea>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="number" name="project_cost" min="1" class="form-control" id="costo_proyecto" placeholder="Ingrese el costo del proyecto">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="project_status" class="form-control">
					<option value="A">Activar</option>
					<option value="B">Bloquear</option>
				</select>
            </div>
        </div>
		<button type="submit" class="btn bg-primary text-white">Agregar proyecto</button>
	</form>
@endsection