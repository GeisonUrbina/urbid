@extends('layouts.external_no_footer_layout')

@section('content')

<div class="container">
          <div class="row">
            <div class="col s12"><h4 class="cyan-text text-darken-2">Términos y condiciones</h4><br></div>
            <div class="col s12">
            <p>
				Bienvenidos a URBID. Este sitio permite conocer los términos de uso y condiciones que se requiere para 
				utilizar este servicio, proporcionando mayor satisfacción en su uso. Al acceder o utilizar el servicio, 
				usted manifiesta haber leído, entendido y aceptado estos términos y condiciones, por lo que es necesario que 
				sean leídas detenidamente.
			</p>

			<h5>Devoluciones y Reembolsos</h5>
			<p>
				URBID es una plataforma intermediaria entre el proveedor de un producto y el cliente, ambos afiliados a la plataforma. URBID será intermediario de todo reclamo
				entre proveedor y cliente por defecto de algún producto. Para lo anterior, el cliente afectado debe comunicarse al número de teléfono (+506) 2275-0553
			</p>
			<p>
				Si el producto entregado se encuentra vencido debe ser reportado al repartidor al momento de recibir el 
				producto, de lo contrario llamar al centro de servicio (+506) 2275-0553. URBID no hace devolución o reembolso de dinero.
			</p>

			<h5>Restricciones de ventas</h5>
			<p>
				URBID se reserva las ventas de los producto que requieran que sean sólo para mayores de 18 años como está estipulado 
			    por ley,por lo que será necesario que el cliente dé prueba de su mayoría de edad, al no presentar o acceder a ese término, el repartidor eliminará la compra.</p>
			<p>
				El pedido será cancelado si la ubicación se encuentra fuera del área de entrega.
			</p>
			<h5>Comunicaciones eléctronicas</h5>
			<p>
				Al utilizar los servicios de URBID, usted da consentimiento para recibir comunicaciones electrónicas por medio de correos electrónicos, mensaje de texto, llamadas teléfonicas, ubicación geográfica, avisos automáticos de dispositivos móviles, por parte de URBID.
			</p>
			<h5>Cuenta</h5>
			<p>
				Los datos serán vinculados con la aplicación sin mayor peligro, obteniendo privacidad entre usted 
				como cliente y la aplicación URBID como lo rige la política de nuestra empresa. 
			</p>
			<p>
				URBID puede bloquear al cliente si hay cancelación constante de pedidos.
			</p>
			<p>
				URBID no se hará responsable si el cliente comparte su cuenta con terceros. Tampoco se hará responsable de cualquier error que usted haya colocado en su cuenta de cliente. Al proporcionar 
				su nombre, número telefónico o correo electrónico usted estará brindando sus datos para poder acceder a la aplicación.
			</p>
			<p>
				<strong> 
					El cliente debe notificar inmediatamente de cualquier violación de seguridad o uso no 
					autorizado de su cuenta.
				</strong>
			</p>
			<h5>Comunidad organizada afiliada</h5>
			<p>
				Una comunidad organizada estará conformada por los siguientes representantes:
					<ul>
						<li>Presidente(a)</li>
						<li>Vicepresidente(a)</li>
						<li>Tesorero(a)</li>
						<li>Secretatio(a)</li>
						<li>Vocal 1,2 y 3</li>
						<li>Fiscal(a)</li>
					</ul>
				Los representantes serán elegidos por la comunidad. La comunidad puede o no estar inscrita ante el DINADECO, por lo que, URBID tomará en cuenta en primera 
				instancia, los representantes inscritos ante DINADECO.
			</p>
			<h5>Proyectos comunitarios</h5>
			<p>URBID será el encargado de registrar en la plataforma todos aquellos proyectos ambientales, bien social y desarrollo de infraestructura definidos por los 
			miembros responsables de las comunidad organizadas afiliadas.</p>
			<p>Todos los aportes comunitarios van a ser gestionados por URBID. Las comunidades organizadas pueden recomendar los servicios de un proveedor que pueda llevar a cabo 
			el desarrollo del proyecto comunitario.
			</p>
			<p>
				Última actualización: 31/05/2020
			</p>
            </div>
          </div>
      </div>


@endsection