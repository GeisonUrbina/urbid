@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Crear campaña</h1>
    <hr/>
    @if(session()->has('success') || session()->has('error'))
	   	<div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
            <p>{{ session()->get('error') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
		  	@foreach ($errors->all() as $error)
			    <p>{{ $error }}</p>
		    @endforeach
		  </div>
		</div>
	@endif
	<form method="post" action="new-advertisement" enctype="multipart/form-data">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-6 mb-3 mb-sm-0">
				<input type="text" name="advertisement_code" class="form-control" id="codigo_campana" placeholder="Ingrese el código de la campaña">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="advertisement_image" type="file" accept="image/*" id="imagen_campana">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="advertisement_name" class="form-control" id="nombre_campana" placeholder="Ingrese el nombre de la campaña">
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="advertisement_priority" class="form-control">
					<option>Seleccione prioridad</option>
					@for($i = 1;$i < 11;$i++)
						<option value="{{$i}}">{{$i}}</option>
					@endfor
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="advertisement_warehouse_code" class="form-control">
					<option value="">Todas las comunidades</option>
					@foreach($warehouses as $warehouse)
						<option value="{{$warehouse->warehouse_code}}">{{$warehouse->warehouse_name}}</option>
					@endforeach
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="advertisement_category" class="form-control">
					<option>Seleccione categoría</option>
					    <option value="PUBLICIDAD">Publicidad</option>
                        <option value="PROMOCION">Promoción</option>
                        <option value="ACTIVIDAD_COMUNITARIA">Actividad comunitaria</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="advertisement_location" class="form-control">
					    <option >Seleccione ubicación</option>
					    <option value="PRINCIPAL">Principal</option>
                        <option value="ABAJO">Abajo</option>
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<select name="advertisement_status" class="form-control">
					<option value="A">Activar</option>
					<option value="B">Bloquear</option>
				</select>
            </div>
        </div>
		<button type="submit" class="btn bg-primary text-white">Agregar campaña</button>
	</form>
@endsection