@extends('layouts.external_no_footer_layout')

@section('content')
<body>
  <div class="section"></div>
  <main>
    <center>
      <img class="responsive-img" height="100" width="140" src="/img/logo.jpg" />

      <h5 class="cyan-text">Registre su cuenta</h5>
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
        <form class="col s12" method="post" action="login-client-web">
          @csrf
          <div class="form-group">
            <input type="email" name="client_email" class="form-control" value="{{old('client_email')}}" placeholder="Correo eléctronico">
            <input type="password" name="client_password" class="form-control" placeholder="Contraseña">
          </div>
          <button type="submit" class="btn btn-default" style="margin:10px;">Ingresar</button>
        </form>
        </div>
      </div>
    </center>
    <div class="section"></div>
    <div class="section"></div>
  </main>
@endsection