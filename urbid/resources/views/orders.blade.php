@extends('layouts.internal_layout')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Pedidos solicitados</h1>
    <hr/>


         <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Código</th>
                      <th>Fecha</th>
                      <th>Monto Total</th>
                      <th>Monto Pago</th>
                      <th>Monto Vuelto</th>
                      <th>Repartidor</th>
                      <th>Forma de pago</th>
                      <th>Estado</th>
                      <th>Productos</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@for($i=0; $i < count($listaCompras);$i++)
						<tr>
	                      <td>{{$listaCompras[$i]->codigo}}</td>
	                      <td>{{$listaCompras[$i]->created_at}}</td>
	                      <td>{{number_format($listaCompras[$i]->montoTotal + $listaCompras[$i]->montoEnvio,2,'.',',')}}</td>
	                      <td>{{number_format($listaCompras[$i]->montoPagar,2,'.',',')}}</td>
	                      <td>
	                      	@if($listaCompras[$i]->forma_pago == 'E') 
								{{number_format($listaCompras[$i]->montoPagar - ($listaCompras[$i]->montoTotal + $listaCompras[$i]->montoEnvio),2,'.',',')}}
							@else
								{{number_format(0,2,'.',',')}}
	                      	@endif
	                      </td>
	                      <td>
	                      	<form method="post" action="modify-order-delivery">
		                    	@csrf
		                    	<input type="hidden" value="{{$listaCompras[$i]->codigo}}" name="orderCode" />
								<select name="repartidor" id="repartidor" onchange="this.form.submit()" class="form-control">
									<option value="">Seleccionar</option>	
									@foreach ($deliveryPeople as $person)
										<option value="{{$person->id}}" @if($listaCompras[$i]->id_repartidor == $person->id) selected @endif>{{$person->delivery_person_name}}</option>
								    @endforeach	
								</select>
	                      	</form>
	                      </td>
						  <td>
						  	@if($listaCompras[$i]->forma_pago == 'E') Efectivo @endif
							@if($listaCompras[$i]->forma_pago == 'P') Puntos @endif
							@if($listaCompras[$i]->forma_pago == 'S') Sinpe @endif
						  </td>
						  <td>
						  	<form method="post" action="modify-order-status">
						  		@csrf
						  		<input type="hidden" value="{{$listaCompras[$i]->codigo}}" name="orderCode" />
						  		<select name="estado" id="estado" onchange="this.form.submit()" class="form-control">
									<option value="PC" @if($listaCompras[$i]->status == 'PC') selected @endif>
										Procesando compra
									</option>	
									<option value="PE" @if($listaCompras[$i]->status == 'PE') selected @endif>
										Pendiente entrega
									</option>		
								</select>
						  	</form>
						  </td>
						  <td><a href="/order-detail?repartidor={{$listaCompras[$i]->id_repartidor}}&pedido={{$listaCompras[$i]->codigo}}">Ver detalle</a></td>
	                    </tr>
                  	@endfor
                  </tbody>
                </table>
                
                @if(count($listaCompras) == 0)
	                <div class="col-lg-12 mb-2">
		                  <div class="card bg-danger text-white shadow">
		                    <div class="card-body">
		                      No hay pedidos solicitados
		                    </div>
		                  </div>
		            </div>
	            @endif
              </div>
            </div>
          </div>
@endsection