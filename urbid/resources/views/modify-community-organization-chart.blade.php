@extends('layouts.internal_layout')


@section('content')

<h1 class="h3 mb-4 text-gray-800">Modificar organigrama</h1>
    <hr/>
	@if(session()->has('success'))
	    <div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
			  	@foreach ($errors->all() as $error)
				    <p>{{ $error }}</p>
			    @endforeach
		  </div>
		</div>
	@endif
	<form class="navbar-form" method="get" action="search_organization_chart_by_warehouse_code">
		@csrf
		<div class="form-group row">
        	<div class="col-sm-3 mb-3 mb-sm-0">
				<input type="text" name="warehouse_code" class="form-control" placeholder="Buscar código de la comunidad">
            </div>
            <button type="submit" class="btn bg-primary text-white">Buscar</button>
        </div>
	</form>
	@if(!empty($organization_chart_data))
		<form method="post" action="update-community-organization-chart" enctype="multipart/form-data">
			@csrf
			<input type="hidden" name="selectedWarehouseCode" value="{{$selectedCode}}"  class="form-control">
			<div class="form-group row">
				<div class="col-sm-6 mb-3 mb-sm-0">
					<input type="text"  name="warehouse_code" class="form-control" id="codigo_comunidad" value="{{$organization_chart_data->warehouse_code}}" readonly>
	            </div>
        	</div>
			<div class="form-group row">
				<div class="col-sm-6">
					<input type="text" name="president_name" class="form-control" id="president_name" value="{{$organization_chart_data->presidente}}" placeholder="Ingrese el nombre del presidente">
				</div>
				<div class="col-sm-3 mb-3 mb-sm-0">
					<input name="president_image" type="file" accept="image/*" id="imagen_producto">
				</div>
        	</div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="president_identification" class="form-control" id="president_identification" value="{{$organization_chart_data->p_identificacion}}" placeholder="Ingrese la identificacion del presidente">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vicepresident_name" class="form-control" id="vicepresident_name" value="{{$organization_chart_data->vicepresidente}}" placeholder="Ingrese el nombre del vicepresidente">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="vicepresident_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vicepresident_identification" class="form-control" id="vicepresident_identification" value="{{$organization_chart_data->v_identificacion}}" placeholder="Ingrese la identificacion del vicepresidente">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="treasurer_name" class="form-control" id="treasurer_name" value="{{$organization_chart_data->tesorero}}" placeholder="Ingrese el nombre del tesorero">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="treasurer_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="treasurer_identification" class="form-control" id="treasurer_identification" value="{{$organization_chart_data->t_identificacion}}" placeholder="Ingrese la identificacion del tesorero">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="secretary_name" class="form-control" id="secretary_name" value="{{$organization_chart_data->secretario}}" placeholder="Ingrese el nombre del secretario">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="secretary_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="secretary_identification" class="form-control" id="secretary_identification" value="{{$organization_chart_data->s_identificacion}}" placeholder="Ingrese la identificacion del secretario">
            </div>
        </div>


        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal1_name" class="form-control" id="vocal1_name" value="{{$organization_chart_data->vocal_1}}" placeholder="Ingrese el nombre del vocal 1">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="vocal1_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal1_identification" class="form-control" id="vocal1_identification" value="{{$organization_chart_data->v1_identificacion}}" placeholder="Ingrese la identificacion del vocal 1">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal2_name" class="form-control" id="vocal2_name" value="{{$organization_chart_data->vocal_2}}" placeholder="Ingrese el nombre del vocal 2">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="vocal2_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal2_identification" class="form-control" id="vocal2_identification" value="{{$organization_chart_data->v2_identificacion}}" placeholder="Ingrese la identificacion del vocal 2">
            </div>
        </div>


        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal3_name" class="form-control" id="vocal3_name" value="{{$organization_chart_data->vocal_3}}" placeholder="Ingrese el nombre del vocal 3">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="vocal3_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal3_identification" class="form-control" id="vocal3_identification" value="{{$organization_chart_data->v3_identificacion}}" placeholder="Ingrese la identificacion del vocal 3">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="fiscal_name" class="form-control" id="fiscal_name" value="{{$organization_chart_data->fiscal}}" placeholder="Ingrese el nombre del fiscal">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="fiscal_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="f_identification" class="form-control" id="fiscal_identification" value="{{$organization_chart_data->f_identificacion}}" placeholder="Ingrese la identificacion del fiscal">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-12 mb-12 mb-sm-0">
				<button type="submit" class="btn bg-gradient-success btn-user text-white">Modificar organigrama</button>
			</div> 
        </div>
		</form>
	@endif

@endsection