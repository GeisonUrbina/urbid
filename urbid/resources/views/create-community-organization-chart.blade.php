@extends('layouts.internal_layout')


@section('content')

<h1 class="h3 mb-4 text-gray-800">Crear organigrama</h1>
    <hr/>
	@if(session()->has('success'))
	    <div class="panel green text-color-white">
		  <div class="panel-body">
		    <p>{{ session()->get('success') }}</p>
		  </div>
		</div>
	@endif
	@if ($errors->any())
		<div class="panel red text-color-red">
		  <div class="panel-body">
			  	@foreach ($errors->all() as $error)
				    <p>{{ $error }}</p>
			    @endforeach
		  </div>
		</div>
	@endif
    <form method="post" action="new-community-organization-chart" enctype="multipart/form-data">
		@csrf
        <div class="form-group row">
        	<div class="col-sm-6 mb-6 mb-sm-0">
				<select name="warehouse_code" class="form-control">
				    <option value="select">Seleccione la comunidad</option>
					@foreach ($warehouses as $warehouse)
						<option value="{{$warehouse->warehouse_code}}">{{$warehouse->warehouse_name}}</option>
				    @endforeach
				</select>
            </div>
        </div>
        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="president_name" class="form-control" id="president_name" placeholder="Ingrese el nombre del presidente">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="president_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="president_identification" class="form-control" id="president_identification" placeholder="Ingrese la identificacion del presidente">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vicepresident_name" class="form-control" id="vicepresident_name" placeholder="Ingrese el nombre del vicepresidente">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="vicepresident_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vicepresident_identification" class="form-control" id="vicepresident_identification" placeholder="Ingrese la identificacion del vicepresidente">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="treasurer_name" class="form-control" id="treasurer_name" placeholder="Ingrese el nombre del tesorero">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="treasurer_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="treasurer_identification" class="form-control" id="treasurer_identification" placeholder="Ingrese la identificacion del tesorero">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="secretary_name" class="form-control" id="secretary_name" placeholder="Ingrese el nombre del secretario">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="secretary_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="secretary_identification" class="form-control" id="secretary_identification" placeholder="Ingrese la identificacion del secretario">
            </div>
        </div>


        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal1_name" class="form-control" id="vocal1_name" placeholder="Ingrese el nombre del vocal 1">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="vocal1_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal1_identification" class="form-control" id="vocal1_identification" placeholder="Ingrese la identificacion del vocal 1">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal2_name" class="form-control" id="vocal2_name" placeholder="Ingrese el nombre del vocal 2">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="vocal2_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal2_identification" class="form-control" id="vocal2_identification" placeholder="Ingrese la identificacion del vocal 2">
            </div>
        </div>


        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal3_name" class="form-control" id="vocal3_name" placeholder="Ingrese el nombre del vocal 3">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="vocal3_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="vocal3_identification" class="form-control" id="vocal3_identification" placeholder="Ingrese la identificacion del vocal 3">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="fiscal_name" class="form-control" id="fiscal_name" placeholder="Ingrese el nombre del fiscal">
            </div>
            <div class="col-sm-3 mb-3 mb-sm-0">
				<input name="fiscal_image" type="file" accept="image/*" id="imagen_producto">
            </div>
        </div>

        <div class="form-group row">
        	<div class="col-sm-6">
				<input type="text" name="fiscal_identification" class="form-control" id="fiscal_identification" placeholder="Ingrese la identificacion del fiscal">
            </div>
        </div>


        <div class="form-group row">
        	<div class="col-sm-12 mb-12 mb-sm-0">
				<button type="submit" class="btn bg-gradient-success btn-user text-white">Agregar organigrama</button>
			</div> 
        </div>
	</form>
@endsection