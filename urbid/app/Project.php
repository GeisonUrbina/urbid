<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'project_code',
        'project_name',
        'project_description',
        'project_cost',
        'project_collected_amount',
        'warehouse_code',
        'project_status',
        'project_image',
    ];

}
