<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = [
        'warehouse_code', 'warehouse_country','province_code','canton_code','district_code','warehouse_name','warehouse_workers','warehouse_status',
    ];

    public $incrementing = 'false';
    public $timestamps = false;
}
