<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = [
        'district_code',
        'district_name',
        'canton_code',
        'postal_code',
    ];

    public $incrementing = 'false';
    public $timestamps = 'false';
}
