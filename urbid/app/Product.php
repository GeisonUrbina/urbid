<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product_code','product_name','made_in','product_description','product_cost','product_com_profit','product_quantity','is_offer','subcategory_code','product_status','product_image',
    ];

    public $incrementing = 'false';
}
