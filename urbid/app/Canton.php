<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Canton extends Model
{
    protected $fillable = [
        'canton_code', 'canton_name','province_code',
    ];

    public $incrementing = 'false';
    public $timestamps = 'false';
}
