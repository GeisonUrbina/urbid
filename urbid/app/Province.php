<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = [
        'province_code',
        'province_name',
    ];

    public $incrementing = 'false';
    public $timestamps = 'false';
}
