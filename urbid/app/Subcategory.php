<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = [
        'subcategory_code', 'category_code','subcategory_name','subcategory_status','subcategory_image',
    ];

    protected $primaryKey = 'subcategory_code';
    protected $keyType = 'string';
    public $incrementing = 'false';
}
