<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shopping extends Model
{
    protected $fillable = [
      'codigo',
      'montoTotal',
      'montoPagar',
      'montoEnvio',
      'forma_pago',
      'id_usuario',
      'id_repartidor',
      'status',
      'carrito',
      'latitud',
      'longitud',
    ];
}
