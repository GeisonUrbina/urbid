<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryPerson extends Model
{
    protected $fillable = [
        'delivery_person_name',
        'delivery_person_lastName',
        'picture',
        'delivery_person_identificationNumber',
        'delivery_person_identificationType',
        'delivery_person_phone',
        'player_id',
        'phone_code',
        'phone_validated',
        'delivery_person_email',
        'validation_method',
        'password',
        'delivery_person_role',
        'delivery_person_status',
        'total_viajes'
    ];
}
