<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectIncome extends Model
{
    protected $fillable = [
        'id', 'project_code','client_id','shopping_code','income_type','income',
    ];

}
