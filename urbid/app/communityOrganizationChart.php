<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class communityOrganizationChart extends Model
{
    protected $fillable = [
        'warehouse_code','presidente','p_imagen','p_identificacion','vicepresidente','v_imagen',
        'v_identificacion','tesorero','t_imagen','t_identificacion','secretario','s_imagen',
        's_identificacion','vocal_1','v1_imagen','v1_identificacion','vocal_2','v2_imagen',
        'v2_identificacion','vocal_3','v3_imagen','v3_identificacion','fiscal','f_imagen',
        'f_identificacion'
    ];
}
