<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'https://www.urbidcr.com/administration/*',
        'https://www.urbidcr.com/administration/category/*',
        'https://www.urbidcr.com/administration/product/*',
        'https://www.urbidcr.com/administration/client/*',
        'https://www.urbidcr.com/administration/cart/*',
        'https://www.urbidcr.com/administration/receipt/*',
        'https://www.urbidcr.com/administration/shopping/*',
        'https://www.urbidcr.com/app/*'
    ];
}
