<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
use Closure;

class Login
{
    protected $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if($this->auth->user()->client_role == 'PA'){
            return redirect()->to('/');
        }else if($this->auth->user()->client_role == 'CL'){
            return redirect()->to('/client-orders');
        }else if($this->auth->user()->client_role == 'AD'){
            return redirect()->to('/orders');
        }else{
            return redirect()->to('/');
        }
        return $next($request);
    }
}