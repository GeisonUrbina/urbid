<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DB;

use DeliveryPerson;

class DeliveryPersonController extends Controller
{
    public function create(Request $request){

        $reglas = array(
            'name' => 'required',
            'lastName' => 'required',
            'identificationType' => 'required',
            'identificationNumber' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'state' => 'required',
            'password' => 'required|min:8|string'
            );

    	$validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }

            \App\DeliveryPerson::create([
                'delivery_person_name' => $request['name'],
                'delivery_person_lastName' =>$request['lastName'],
                'picture' => '',
                'delivery_person_identificationNumber' => $request['identificationNumber'],
                'delivery_person_identificationType' => $request['identificationType'],
                'delivery_person_phone' => $request['phone'],
                'player_id' => "",
                'phone_code' => "",
                'phone_validated' => true,
                'delivery_person_email' => $request['email'],
                'delivery_person_role' => 'RE',
                'validation_method' => 'T',
                'delivery_person_status' => $request['state'],
                'password' => bcrypt($request['user_password']),
                'total_viajes' => 0
            ]);


            return redirect()->back()->with('success', 'El repartidor se ha creado exitosamente!');
    }

    public function show(Request $request){
        $delivery_person_data = \App\DeliveryPerson::where('id',$request->repartidor)->first();
        $deliveryPeople = DB::table('delivery_people')->get();
        return view('modify-delivery-person',compact('deliveryPeople','delivery_person_data'));
    }

    public function update(Request $request){
        $reglas = array(
            'id' => 'required',
            'nombre' => 'required',
            'apellido' => 'required',
            'tipo_identificacion' => 'required',
            'numero_identificacion' => 'required',
            'correo_electronico' => 'required|email',
            'numero_celular' => 'required',
            'estado' => 'required',
            'contrasena' => 'nullable|min:8|string'
            );

        $validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }

        DB::table('delivery_people')->where('id',$request->id)->update([
            'delivery_person_name' => $request->nombre,
            'delivery_person_lastName' => $request->apellido,
            'delivery_person_identificationNumber' => $request->numero_identificacion,
            'delivery_person_identificationType' => $request->tipo_identificacion,
            'delivery_person_phone' => $request->numero_celular,
            'delivery_person_email' => $request->correo_electronico,
            'delivery_person_status' => $request->estado
        ]);

        if($request->contrasena){
            DB::table('delivery_people')->where('id',$request->id)->update([
            'password' => bcrypt($request->contrasena),
        ]);
        }

        return redirect()->back()->with('success', 'El repartidor se ha modificado exitosamente!');
    }
}
