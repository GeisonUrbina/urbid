<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Crypt;

use User;

use DB;

use Auth;

use Illuminate\Support\Facades\Middleware;

use GuzzleHttp\Client;

use Urbid\Http\Controllers\NotificationController;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['logoutWeb']]);
        $this->middleware('admin', ['only' => ['update']]);
    }

    public function update($client_email,$client_status){
    	DB::table('users')->where('email',$client_email)->update(['client_status'=>$client_status]);
    	return redirect()->back()->with('success', 'El usuario se ha creado exitosamente! Pronto recibirá el correo de confirmación');
    }

    public function loginWeb(Request $request){

        $reglas = array(
            'client_email' => 'required',
            'client_password' => 'required'
            );

        $validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }

        if(Auth::attempt(['email' => $request->client_email,'password' => $request->client_password,'client_status' => 'A'])){
            return redirect('routing');
        }else{
            return redirect()->back()->withInput();
        }
        
    }

    public function logoutWeb(){
        Auth::logout();

        return redirect('/')->header('Cache-Control', 'no-store, no-cache, must-revalidate');
    }

}
