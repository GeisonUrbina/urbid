<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Subcategory;
use DB;

class SubcategoryController extends Controller
{
    public function create(Request $request){
        $rutaImagen = "";

        $reglas = array(
            'subcategory_code' => 'string|unique:subcategories|max:80|min:1|required',
            'subcategory_category_index' => 'string|max:80|min:1|required',
            'subcategory_name' => 'string|max:80|min:1|required',
            'subcategory_status' => 'required|min:1|max:3',
            'subcategory_image' => 'image|mimes:jpeg,png,jpg|required',
            );

    	$validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }

        if($request->file('subcategory_image')){
            $rutaImagen = $this->imageGenerator($request);
        }
        

    	 \App\Subcategory::create([
            'subcategory_code' => $request['subcategory_code'],
            'category_code' => $request['subcategory_category_index'],
            'subcategory_name' => $request['subcategory_name'],
            'subcategory_status' => $request['subcategory_status'],
            'subcategory_image' => $rutaImagen
            ]);

    	return redirect()->back()->with('success', 'La subcategoría se ha creado exitosamente!');
    }

    public function update(Request $request){
    	//Declaracion de variables
        $rutaImagen = "";

        //Se realiza la validación correspondiente
    	$this->validate($request, [
	        'subcategory_code' => 'string|max:80|min:1|required',
	        'subcategory_category_index' => 'string|max:80|min:1|required',
            'subcategory_name' => 'string|max:80|min:1|required',
            'subcategory_status' => 'required|min:1|max:3',
            'subcategory_image' => 'image|mimes:jpeg,png,jpg',
    	]);

    	/**AQUI CREAR UN HASH PARA LA IMAGEN**/

        if($request->file('subcategory_image')){
            $rutaImagen = $this->imageGenerator($request);
        }


    	/**AQUI HACER LA INSERCION A LA BASE DE DATOS**/
        if($rutaImagen != null && $rutaImagen != ""){
            DB::table('subcategories')->where('subcategory_code',$request->subcategory_code)->update(['subcategory_name'=>$request->subcategory_name,'category_code'=>$request->subcategory_category_index,'subcategory_status'=>$request->subcategory_status,'subcategory_image'=>$rutaImagen]);
        }else{
            DB::table('subcategories')->where('subcategory_code',$request->subcategory_code)->update(['subcategory_name'=>$request->subcategory_name,'category_code'=>$request->subcategory_category_index,'subcategory_status'=>$request->subcategory_status]);
        }

    	return redirect()->back()->with('success', 'La subcategoría se ha modificado exitosamente!')->withInput();
   
    }

    public function show(Request $request){
    	$subcategory_data = \App\Subcategory::where('subcategory_code', $request->search_subcategory_code)->first();
        $categories = DB::table('categories')->select('category_code', 'category_name')->get();
        return view('modify-subcategory',compact('subcategory_data','categories'));
    }


    private function imageGenerator($request){
        $rutaImagen = "";
         if($request->file('subcategory_image')->getClientOriginalExtension() == "jpeg" || 
            $request->file('subcategory_image')->getClientOriginalExtension() == "jpg" || 
            $request->file('subcategory_image')->getClientOriginalExtension() == "png" ||
            $request->file('subcategory_image')->getClientOriginalExtension() == "PNG" || 
            $request->file('subcategory_image')->getClientOriginalExtension() == "JPG"){
                $nombreImagen= md5(gmdate('Y-m-d h:i:s \G\M\T'));
                $original;
                if($request->file('subcategory_image')->getClientOriginalExtension() == "png" || 
                    $request->file('subcategory_image')->getClientOriginalExtension() == "PNG"){
                    $original = imagecreatefrompng($request->file('subcategory_image')->getRealPath());
                }else{
                    $original = imagecreatefromjpeg($request->file('subcategory_image')->getRealPath());
                }

                $ancho_original = imagesx($original);
                $alto_original = imagesy($original);

                $copia = imagecreatetruecolor(412,240);

                imagecopyresampled($copia, $original, 0,0,0,0,412,240,$ancho_original,$alto_original);

                imagejpeg($copia,public_path('img/subcategory_images')."/".$nombreImagen.".".$request->file('subcategory_image')->getClientOriginalExtension(),100);
               // $request->file('imagen')->move(public_path('imagenes_promociones'),$nombreImagen.".".$request->file('imagen')->getClientOriginalExtension());
                $rutaImagen = $nombreImagen.".".$request->file('subcategory_image')->getClientOriginalExtension();
             
                imagedestroy($original);
                imagedestroy($copia);
        }  

        return $rutaImagen;
    }
}
