<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Category;

use DB;

class CategoryController extends Controller
{
    public function create(Request $request){

        $rutaImagen = "";

        $reglas = array(
            'category_code' => 'string|unique:categories|max:80|min:1|required',
            'category_name' => 'string|max:80|min:1|required',
            'category_status' => 'required|min:1|max:3',
            'category_image' => 'image|mimes:jpeg,png,jpg|required',
            );

    	$validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }

        if($request->file('category_image')){
            $rutaImagen = $this->imageGenerator($request);
        }
        

            \App\Category::create([
            'category_code' => $request['category_code'],
            'category_name' => $request['category_name'],
            'category_status' => $request['category_status'],
            'category_image' => $rutaImagen
            ]);

    	return redirect()->back()->with('success', 'La categoría se ha creado exitosamente!');
    }

    public function show(Request $request){

        $category_data = \App\Category::where('category_code', $request->search_category_code)->first();
        return view('modify-category',compact('category_data'));
    }

    public function update(Request $request){
        $rutaImagen = "";

    	$this->validate($request, [
	        'category_code' => 'string|max:80|min:1|required',
            'category_name' => 'string|max:80|min:1|required',
            'category_status' => 'required|min:1|max:3',
            'category_image' => 'image|mimes:jpeg,png,jpg',
    	]);

        if($request->file('category_image')){
            $rutaImagen = $this->imageGenerator($request);
        }

        if($rutaImagen != null && $rutaImagen != ""){
            DB::table('categories')->where('category_code',$request->category_code)->update(['category_name'=>$request->category_name,'category_status'=>$request->category_status,'category_image'=>$rutaImagen]);
        }else{
            DB::table('categories')->where('category_code',$request->category_code)->update(['category_name'=>$request->category_name,'category_status'=>$request->category_status]);
        }

    	return redirect()->back()->with('success', 'La categoría se ha modificado exitosamente!')->withInput();
    }


    private function imageGenerator($request){
	        $rutaImagen = "";
	         if($request->file('category_image')->getClientOriginalExtension() == "jpeg" || 
	            $request->file('category_image')->getClientOriginalExtension() == "jpg" || 
	            $request->file('category_image')->getClientOriginalExtension() == "png" || 
	            $request->file('category_image')->getClientOriginalExtension() == "PNG" || 
	            $request->file('category_image')->getClientOriginalExtension() == "JPG"){
	                $nombreImagen= md5(gmdate('Y-m-d h:i:s \G\M\T'));
	                if($request->file('category_image')->getClientOriginalExtension() == "png" || 
	            $request->file('category_image')->getClientOriginalExtension() == "PNG"){
	                    $original = imagecreatefrompng($request->file('category_image')->getRealPath());
	                }else{
	                    $original = imagecreatefromjpeg($request->file('category_image')->getRealPath());
	                }
	                
	                $ancho_original = imagesx($original);
	                $alto_original = imagesy($original);

	                $copia = imagecreatetruecolor(412,100);

	                imagecopyresampled($copia, $original, 0,0,0,0,412,100,$ancho_original,$alto_original);

	                imagejpeg($copia,public_path('img/category_images')."/".$nombreImagen.".".$request->file('category_image')->getClientOriginalExtension(),100);
	                $rutaImagen = $nombreImagen.".".$request->file('category_image')->getClientOriginalExtension();
	             
	                imagedestroy($original);
	                imagedestroy($copia);
	        }  

	        return $rutaImagen;
	    }

}
