<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Warehouse;

use communityOrganizationChart;

use DB;

class WarehouseController extends Controller
{
	public function create(Request $request){
    	
        $reglas = array(
            'warehouse_code' => 'string|unique:warehouses|max:80|min:1|required',
            'warehouse_country' => 'string|max:80|min:1|required',
            'province_code' => 'string|exists:provinces|max:80|min:1|required',
            'canton_code' => 'string|exists:cantons|max:80|min:1|required',
            'district_code' => 'string|exists:districts|max:80|min:1|required',
            'warehouse_name' => 'string|max:80|min:1|required',
            'warehouse_workers' => 'integer|min:1|required',
            'warehouse_status' => 'required|min:1|max:3',
            );

    	$validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }

            \App\Warehouse::create([
            'warehouse_code' => $request['warehouse_code'],
            'warehouse_country' => $request['warehouse_country'],
            'province_code' => $request['province_code'],
            'canton_code' => $request['canton_code'],
            'district_code' => $request['district_code'],
            'warehouse_name' => $request['warehouse_name'],
            'warehouse_workers' => $request['warehouse_workers'],
            'warehouse_status' => $request['warehouse_status']
            ]);

    	return redirect()->back()->with('success', 'El almacen se ha creado exitosamente!');
    }

    public function update(Request $request){
       
       //Se crean las reglas con las que se van a validar los campos enviados desde el formulario
        $reglas = array(
            'warehouse_code' => 'string|max:80|min:1|required',
            'warehouse_country' => 'string|max:80|min:1|required',
            'province_code' => 'string|exists:provinces|max:80|min:1|required',
            'canton_code' => 'string|exists:cantons|max:80|min:1|required',
            'district_code' => 'string|exists:districts|max:80|min:1|required',
            'warehouse_name' => 'string|max:80|min:1|required',
            'warehouse_workers' => 'integer|min:1|required',
            'warehouse_status' => 'required|min:1|max:3',
            );

        //Se realiza la validación correspondiente
        $validacion = Validator::make($request->all(),$reglas);

        //Se comprueba si fallo la validación, si es el caso se retorna a la vista con errores
        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }

        /**AQUI HACER LA INSERCION A LA BASE DE DATOS**/

        DB::table('warehouses')->where('warehouse_code',$request->warehouse_code)->update([
            'warehouse_country'=>$request->warehouse_country,
            'province_code'=>$request->province_code,
            'canton_code'=>$request->canton_code,
            'district_code'=>$request->district_code,
            'warehouse_name'=>$request->warehouse_name,
            'warehouse_workers'=>$request->warehouse_workers,
            'warehouse_status'=>$request->warehouse_status
            ]);

        return redirect()->back()->with('success', 'La bodega se ha modificado exitosamente!')->withInput();
    }

    public function showCantons(Request $request){
    	$reglas = array(
            'province_code' => 'string|exists:provinces|required',
            );

    	$validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
           return response()->json(['error' => 'NOT FOUND'], 404);
        }

        $cantons = DB::table('cantons')->where('province_code', $request->province_code)->get();

        $response = array(
          'status' => 'success',
          'response' => $cantons,
      );

        return $cantons;
    }

    public function showDistricts(Request $request){
    	$reglas = array(
            'canton_code' => 'string|exists:cantons|required',
            );

    	$validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return response()->json(['error' => 'NOT FOUND'], 404);
        }

        $districts = DB::table('districts')->where('canton_code', $request->canton_code)->get();

        $response = array(
          'status' => 'success',
          'response' => $districts,
      );

        return $districts;
    }

    public function show(Request $request){
        $warehouse_data = \App\Warehouse::where('warehouse_code', $request->search_warehouse_code)->first();
        $provinces = DB::table('provinces')->select('province_code','province_name')->get();
        $cantons = DB::table('cantons')->select('canton_code','canton_name')->get();
        $districts = DB::table('districts')->select('district_code','district_name')->get();

        return view('modify-warehouse',compact('warehouse_data','provinces','cantons','districts'));
    }

    public function showAll(Request $request){
        
        $warehouses_data = \App\Warehouse::where('warehouse_status','A')->get();
        return $warehouses_data;
    }

    public function createCommunityOrganizationChart(Request $request){
        $reglas = array(
            'warehouse_code' => 'required',
            'president_name' => 'required',
            'vicepresident_name' => 'required',
            'treasurer_name' => 'required',
            'secretary_name' => 'required',
            'vocal1_name' => 'required',
            'vocal2_name' => 'required',
            'vocal3_name' => 'required',
            'fiscal_name' => 'required',
            );

    	$validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }

            $rutaImagenP = $this->getImageRoute($request->selectedWarehouseCode,$request->file('president_image'),'p_imagen');
            $rutaImagenV = $this->getImageRoute($request->selectedWarehouseCode,$request->file('vicepresident_image'),'v_imagen');
            $rutaImagenT = $this->getImageRoute($request->selectedWarehouseCode,$request->file('treasurer_image'),'t_imagen');
            $rutaImagenS = $this->getImageRoute($request->selectedWarehouseCode,$request->file('secretary_image'),'s_imagen');
            $rutaImagenV1 = $this->getImageRoute($request->selectedWarehouseCode,$request->file('vocal1_image'),'v1_imagen');
            $rutaImagenV2 = $this->getImageRoute($request->selectedWarehouseCode,$request->file('vocal2_image'),'v2_imagen');
            $rutaImagenV3 = $this->getImageRoute($request->selectedWarehouseCode,$request->file('vocal3_image'),'v3_imagen');
            $rutaImagenF = $this->getImageRoute($request->selectedWarehouseCode,$request->file('fiscal_image'),'f_imagen');

            \App\communityOrganizationChart::create([
                'warehouse_code' => $request['warehouse_code'],
                'presidente' => $request['president_name'],
                'p_identificacion' => $request['president_identification'],
                'p_imagen' => $rutaImagenP,
                'vicepresidente' => $request['vicepresident_name'],
                'v_identificacion' => $request['vicepresident_identification'],
                'v_imagen' => $rutaImagenV,
                'tesorero' => $request['treasurer_name'],
                't_identificacion' => $request['treasurer_identification'],
                't_imagen' => $rutaImagenT,
                'secretario' => $request['secretary_name'],
                's_identificacion' => $request['secretary_identification'],
                's_imagen' => $rutaImagenS,
                'vocal_1' => $request['vocal1_name'],
                'v1_identificacion' => $request['vocal1_identification'],
                'v1_imagen' => $rutaImagenV1,
                'vocal_2' => $request['vocal2_name'],
                'v2_identificacion' => $request['vocal2_identification'],
                'v2_imagen' => $rutaImagenV2,
                'vocal_3' => $request['vocal3_name'],
                'v3_identificacion' => $request['vocal3_identification'],
                'v3_imagen' => $rutaImagenV3,
                'fiscal' => $request['fiscal_name'],
                'f_identificacion' => $request['f_identification'],
                'f_imagen' => $rutaImagenF
            ]);

    	return redirect()->back()->with('success', 'El organigrama se ha creado exitosamente!');
    }

    public function organizationChartByWarehouseCode(Request $request){
        
        $organization_chart_data = \App\communityOrganizationChart::where('warehouse_code',$request->warehouse_code)->first();
        $warehouses = $this->showAll($request);
        $selectedCode = $request->warehouse_code;
        return view('modify-community-organization-chart',compact('organization_chart_data','warehouses','selectedCode'));
    }

    public function updateCommunityOrganizationChart(Request $request){
        $reglas = array(
            'warehouse_code' => 'required',
            'president_name' => 'required',
            'vicepresident_name' => 'required',
            'treasurer_name' => 'required',
            'secretary_name' => 'required',
            'vocal1_name' => 'required',
            'vocal2_name' => 'required',
            'vocal3_name' => 'required',
            'fiscal_name' => 'required',
            );

    	$validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }
 
        $rutaImagenP = $this->getImageRoute($request->selectedWarehouseCode,$request->file('president_image'),'p_imagen');
        $rutaImagenV = $this->getImageRoute($request->selectedWarehouseCode,$request->file('vicepresident_image'),'v_imagen');
        $rutaImagenT = $this->getImageRoute($request->selectedWarehouseCode,$request->file('treasurer_image'),'t_imagen');
        $rutaImagenS = $this->getImageRoute($request->selectedWarehouseCode,$request->file('secretary_image'),'s_imagen');
        $rutaImagenV1 = $this->getImageRoute($request->selectedWarehouseCode,$request->file('vocal1_image'),'v1_imagen');
        $rutaImagenV2 = $this->getImageRoute($request->selectedWarehouseCode,$request->file('vocal2_image'),'v2_imagen');
        $rutaImagenV3 = $this->getImageRoute($request->selectedWarehouseCode,$request->file('vocal3_image'),'v3_imagen');
        $rutaImagenF = $this->getImageRoute($request->selectedWarehouseCode,$request->file('fiscal_image'),'f_imagen');

           $status =  \App\communityOrganizationChart::where('warehouse_code',$request->selectedWarehouseCode)->update([
            'warehouse_code' => $request['warehouse_code'],
            'presidente' => $request['president_name'],
            'p_identificacion' => $request['president_identification'],
            'vicepresidente' => $request['vicepresident_name'],
            'v_identificacion' => $request['vicepresident_identification'],
            'tesorero' => $request['treasurer_name'],
            't_identificacion' => $request['treasurer_identification'],
            'secretario' => $request['secretary_name'],
            's_identificacion' => $request['secretary_identification'],
            'vocal_1' => $request['vocal1_name'],
            'v1_identificacion' => $request['vocal1_identification'],
            'vocal_2' => $request['vocal2_name'],
            'v2_identificacion' => $request['vocal2_identification'],
            'vocal_3' => $request['vocal3_name'],
            'v3_identificacion' => $request['vocal3_identification'],
            'fiscal' => $request['fiscal_name'],
            'f_identificacion' => $request['f_identification'],
            ]);

        
    	return redirect()->back()->with('success', 'El organigrama se ha modificado exitosamente!')->withInput();
    }

    private function getImageRoute($warehouseCode,$uploadedImage,$column){
        $route = "";
        if($uploadedImage){
            $route = $this->imageGenerator($uploadedImage,$column);
            \App\communityOrganizationChart::where('warehouse_code',$warehouseCode)->update([
                ''.$column => $route
            ]);
        }
        return $route;
    }

    private function imageGenerator($uploadedImage,$column){
        
        $rutaImagen = "";
         if($uploadedImage->getClientOriginalExtension() == "jpeg" || 
            $uploadedImage->getClientOriginalExtension() == "jpg" || 
            $uploadedImage->getClientOriginalExtension() == "png" || 
            $uploadedImage->getClientOriginalExtension() == "JPG"){
                $nombreImagen= md5(gmdate('Y-m-d h:i:s.u \G\M\T') . $column);
                $original = imagecreatefromjpeg($uploadedImage->getRealPath());
                $ancho_original = imagesx($original);
                $alto_original = imagesy($original);

                $copia = imagecreatetruecolor(300,300);

                imagecopyresampled($copia, $original, 0,0,0,0,300,300,$ancho_original,$alto_original);

                imagejpeg($copia,public_path('img/organization_chart_members_images')."/".$nombreImagen.".".$uploadedImage->getClientOriginalExtension(),100);
               // $request->file('imagen')->move(public_path('imagenes_promociones'),$nombreImagen.".".$request->file('imagen')->getClientOriginalExtension());
                $rutaImagen = $nombreImagen.".".$uploadedImage->getClientOriginalExtension();
             
                imagedestroy($original);
                imagedestroy($copia);
        }  

        return $rutaImagen;
    }
}
