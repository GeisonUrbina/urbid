<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Middleware;
use Illuminate\Support\Facades\Validator;
use DB;

use Advertisement;

class AdvertisementController extends Controller
{
    public function create(Request $request){
            $rutaImagen = "";
            $reglas = array(
                'advertisement_code' => 'required',
                'advertisement_image' => 'required',
                'advertisement_name' => 'required',
                'advertisement_priority' => 'required',
                'advertisement_category' => 'required',
                'advertisement_location' => 'required',
                'advertisement_status' => 'required'
            );

            $validacion = Validator::make($request->all(),$reglas);

            if($validacion->fails()){
                return redirect()->back()->withInput()->withErrors($validacion->errors());
            }
            
            if($request->file('advertisement_image')){
                $rutaImagen = $this->imageGenerator($request->file('advertisement_image'));
            }

            

                try{
                \App\Advertisement::create([
                    'code' => $request['advertisement_code'],
                    'name' =>$request['advertisement_name'],
                    'priority' => $request['advertisement_priority'],
                    'category' => $request['advertisement_category'],
                    'position' => $request['advertisement_location'],
                    'status' => $request['advertisement_status'],
                    'warehouse_code' => $request['advertisement_warehouse_code'],
                    'image' => $rutaImagen,
                ]);
            }
            catch(\Illuminate\Database\QueryException $ex){ 
                error_log($ex->getMessage()); 
                return redirect()->back()->with('error', 'ERROR: No se creo la campaña');
            }

            return redirect()->back()->with('success', 'La campaña se ha creado exitosamente!');
    }

    public function show(Request $request){
        $advertisement_data;
        $warehouses = DB::table('warehouses')->where('warehouse_status','A')->get();
        try{
            $advertisement_data = \App\Advertisement::where('code',$request['search_advertisement_code'])->first();
        }catch(\Illuminate\Database\QueryException $ex){ 
            error_log($ex->getMessage());
        }
        return view('modify-advertisement',compact('advertisement_data','warehouses'));
    }

    public function update(Request $request){
        $rutaImagen = "";
        $reglas = array(
            'advertisement_code' => 'required',
            'advertisement_name' => 'required',
            'advertisement_priority' => 'required',
            'advertisement_category' => 'required',
            'advertisement_location' => 'required',
            'advertisement_status' => 'required'
        );

        $validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }
        
        if($request->file('advertisement_image')){
            $rutaImagen = $this->imageGenerator($request->file('advertisement_image'));
        }

        if($rutaImagen != null && $rutaImagen != ""){
            try{
                \App\Advertisement::where('code',$request['advertisement_code'])->update([
                    'name' =>$request['advertisement_name'],
                    'priority' => $request['advertisement_priority'],
                    'category' => $request['advertisement_category'],
                    'position' => $request['advertisement_location'],
                    'status' => $request['advertisement_status'],
                    'warehouse_code' => $request['advertisement_warehouse_code'],
                    'image' => $rutaImagen
                ]);
            }
            catch(\Illuminate\Database\QueryException $ex){
                return redirect()->back()->with('error', 'ERROR: No se creo la campaña');
            }
        }else{

            try{
                \App\Advertisement::where('code',$request['advertisement_code'])->update([
                    'name' =>$request['advertisement_name'],
                    'priority' => $request['advertisement_priority'],
                    'category' => $request['advertisement_category'],
                    'position' => $request['advertisement_location'],
                    'status' => $request['advertisement_status'],
                    'warehouse_code' => $request['advertisement_warehouse_code']
                ]);
            }
            catch(\Illuminate\Database\QueryException $ex){
                return redirect()->back()->with('error', 'ERROR: No se modifico la campaña');
            }
        }


        return redirect()->back()->with('success', 'La campaña se ha creado exitosamente!');
    }

    private function imageGenerator($uploadedImage){
        $rutaImagen = "";
        
            if($uploadedImage->getClientOriginalExtension() == "jpeg" || 
            $uploadedImage->getClientOriginalExtension() == "jpg" || 
            $uploadedImage->getClientOriginalExtension() == "png" ||
            $uploadedImage->getClientOriginalExtension() == "PNG" ||
            $uploadedImage->getClientOriginalExtension() == "JPG"){
                $nombreImagen= md5(gmdate('Y-m-d h:i:s \G\M\T'));
                $original;
                if($uploadedImage->getClientOriginalExtension() == "png" || $uploadedImage->getClientOriginalExtension() == "PNG"){
                    $original = imagecreatefrompng($uploadedImage->getRealPath());
                }else{
                    $original = imagecreatefromjpeg($uploadedImage->getRealPath());
                }
                $ancho_original = imagesx($original);
                $alto_original = imagesy($original);

                $copia = imagecreatetruecolor(250,200);

                imagecopyresampled($copia, $original, 0,0,0,0,250,200,$ancho_original,$alto_original);
                
                imagejpeg($copia,public_path('img/advertisement_images')."/".$nombreImagen.".".$uploadedImage->getClientOriginalExtension(),100);
               // $request->file('imagen')->move(public_path('imagenes_promociones'),$nombreImagen.".".$request->file('imagen')->getClientOriginalExtension());
                $rutaImagen = $nombreImagen.".".$uploadedImage->getClientOriginalExtension();
             
                imagedestroy($original);
                imagedestroy($copia);
        }  

        return $rutaImagen;
    }
}
