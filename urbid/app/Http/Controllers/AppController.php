<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use User;
use DB;
use GuzzleHttp\Client;

class AppController extends Controller
{

    public function validateCustomer(Request $request){
        $isCreated;
        $mensaje;
        if($request->tipo_login == "F"){
            $isCreated = \App\User::where('facebook_id', $request->facebook_id)->where('client_status','A')->first();
            if($isCreated)
                return $mensaje=array('status_response' => "CREADO",'user' => $isCreated);
            else
                return $mensaje=array('status_response' => "NO_CREADO");
        }else{
            $isCreated = \App\User::where('client_phone', $request->client_phone)->where('validation_method','T')->where('client_status','A')->first();
            if($isCreated){
                return $mensaje=array('status_response' => "CREADO",'user' => $isCreated);
            }else{
                return $mensaje=array('status_response' => "NO_CREADO");
            }
                
        }
    }

    public function updatedClientInfo(Request $request){
        
        $user = \App\User::where('id', $request->client_id)->where('client_status','A')->first();
        
        return $mensaje=array('status_response' => "CREADO",'user' => $user);
        
    }

    public function validateDeliveryPerson(Request $request){
        $isCreated;
        $mensaje;
        
        $isCreated = \App\DeliveryPerson::where('delivery_person_phone', $request->client_phone)->where('validation_method','T')->where('delivery_person_role','RE')->first();
        
        if($isCreated){
            $mensaje=array('status_response' => "CREADO",'user' => $isCreated);
            return $mensaje;
        }else{
            $mensaje=array('status_response' => "NO_CREADO");
            return $mensaje;
        }
    }

    public function requestPhoneCode(Request $request){
            
          $client = new Client();
          $randomNumber = random_int(135200,999999);
            /*$urlMessage = 'https://api.sms506.com/sms/707e248d259fdeb9a0d3eacc46d4b6e9/t='.$request->number.'&m=URBID-'.$randomNumber.' es su codigo de verificacion';
            $respo = $client->get($urlMessage);
            $res = $respo->getBody()->getContents();*/
        return $mensaje=array('token_sms' =>$randomNumber);
    }

    public function createApp(Request $request){

        $reglas = array(
            'user_name' => 'required',
            'user_surname' => 'required',
            'user_phone' => 'required',
            'user_email' => 'required|email',
            'user_password' => 'required'
            );

        $validacion = Validator::make($request->all(),$reglas);
        
        if($validacion->fails()){
            return response()->json([
                'clientAutenticado' => 'FAILURE'
            ]);
        }
        
        $encrypted = password_hash($request->user_password,PASSWORD_BCRYPT,["cost" => 10]);
        
            $nuevoUsuario = \App\User::create([
            'client_name' => $request['user_name'],
            'client_lastName' =>$request['user_surname'],
            'client_phone' => $request['user_phone'],
            'picture' => '',
            'facebook_id' => $request['user_id'],
            'player_id' => $request['user_device'],
            'phone_code' => "",
            'warehouse_code' => $request['user_community'],
            'points'=>0,
            'phone_validated' => true,
            'allow_notifications' => true,
            'email' => $request['user_email'],
            'client_role' => 'CL',
            'validation_method' => $request['create_type'],
            'client_status' => 'A',
            'password' => bcrypt($request['user_password'])
            ]);

            return response()->json([
                'clientAutenticado' => 'SUCCESS',
                'cliente' => $nuevoUsuario
            ]);
    }

    public function clientData(Request $request){
       $dataCliente = DB::table('users')->where('id',$request->id_usuario)->first();
       return $mensaje=array('status_response' => "SUCCESS",'data' => $dataCliente);
    }

    public function modifyName(Request $request){
        DB::table('users')->where('id',$request->id_usuario)->update(['client_name'=>$request->user_name]);
        return $mensaje=array('status_response' => "SUCCESS");
    }

    public function modifySurname(Request $request){

        DB::table('users')->where('id',$request->id_usuario)->update(['client_lastName'=>$request->user_surname]);
        return $mensaje=array('status_response' => "SUCCESS");
    }

    public function modifyPhone(Request $request){

        DB::table('users')->where('id',$request->id_usuario)->update(['client_phone'=>$request->user_phone]);
        return $mensaje=array('status_response' => "SUCCESS");
    }

    public function modifyEmail(Request $request){
        DB::table('users')->where('id',$request->id_usuario)->update(['email'=>$request->user_email]);
        return $mensaje=array('status_response' => "SUCCESS");
    }


    public function modifyCommunity(Request $request){
        \App\Affiliation::where('user_id', $request->id_usuario)->delete();
        DB::table('users')->where('id',$request->id_usuario)->update(['warehouse_code'=>$request->warehouse_code]);
        return $mensaje=array('status_response' => "SUCCESS");
    }

    public function showAllCategories(Request $request){
        $category_data = \App\Category::where('category_status', 'A')->get();
        return $category_data;
    }

    public function showAllPromotions(){
        $product_data = \App\Product::where('is_offer',1)->where('product_status','A')->get();
        return $product_data;
    }

    public function showAllSubcategories($category_code){
        $subcategory_data = \App\Subcategory::where('category_code', $category_code)->where('subcategory_status','A')->get();
        
        return $subcategory_data;
    }

    public function showAllProducts($subcategory_code){
        $product_data = \App\Product::where('subcategory_code',$subcategory_code)->where('product_status','A')->get();
        return $product_data;
    }

    public function productCart(Request $request){
        $listaProductos = array(json_decode($request->cart_products));
        $lista = [];
        foreach($listaProductos as $a){
            foreach($a as $key => $b){
                if($b->cliente == $request->cliente){
                    $elemento = DB::table('products')->where('product_code',$b->codigo)->first();
                    $elemento->product_quantity = $b->cantidad;
                    array_push($lista,$elemento);
                }
            }
        }
        return $lista;
    }

    public function searchProducts(Request $request){

        $product_data = DB::select('select * FROM `products` WHERE ((`product_name` like ?) or (`product_description` like ?)) and `product_status` = ?',['%'.$request->keyWord.'%','%'.$request->keyWord.'%','A']);
        return $product_data;
    }

    public function showAllProjects(Request $request){
        $project_data = \App\Project::where('project_status','!=','B')->get();
        return $project_data;
    }

    public function showProjectsByWarehouse(Request $request){
        $project_data = \App\Project::where('project_status','!=','B')->where('warehouse_code',$request->warehouse_code)->get();
        return $project_data;
    }

    public function showProjectIncomeDetail(Request $request){
        $project_data = \App\ProjectIncome::where('project_code',$request->project_code)->get();
        return $project_data;
    }

    public function isAffiliated(Request $request){
        $isAffiliate = \App\Affiliation::where('user_id', $request->id_usuario)->where('project_code',$request->project_code)->first();

        $mensaje;
        if($isAffiliate){
            $mensaje=array('status_response' => "AFILIADO");
        }else{
            $mensaje=array('status_response' => "DESAFILIADO");
        }

        return $mensaje;
    }

    public function modifyAffiliations(Request $request){
         \App\Affiliation::updateOrCreate(
            ['user_id' => $request->id_usuario],
            ['project_code' => $request->project_code]
        );
        
        $mensaje=array('status_response' => "AFILIADO");
        return $mensaje;
    }

    public function disAffiliate(Request $request){
        \App\Affiliation::where('user_id', $request->id_usuario)->delete();

        $mensaje=array('status_response' => "DESAFILIADO");

        return $mensaje;
    }

    public function receiptList(Request $request){
        
        $listaProductos = array(json_decode($request->cart_products));
        $lista = [];
        foreach($listaProductos as $a){
            foreach($a as $key => $b){
                $elemento = DB::table('products')->where('product_code',$b->codigo)->first();
                $elemento->product_quantity = $b->cantidad;
                $elemento->monto = $b->cantidad * $elemento->product_cost;
                $elemento->montoAporte = $b->cantidad * $elemento->product_com_profit;
                array_push($lista,$elemento);
            }
        }
        $puntos = $this->obtenerPuntosCliente($request->id_cliente);
        $montoTotal = $this->calcularMontoTotal($lista);
        $costoEnvio = 0;
        if($montoTotal < 500){
            $montoTotal = $montoTotal + config('constants.costoEnvio');
            $costoEnvio = config('constants.costoEnvio');
        }
        
        return response()->json(['montoTotal' => $montoTotal,'costoEnvio' => $costoEnvio,'montoAporteTotal' => $this->calcularMontoAporteTotal($lista),'puntos' => $puntos,'productos' => $lista]);
    }

    private function obtenerPuntosCliente($idCliente){
        $dataCliente = DB::table('users')->where('id',$idCliente)->first();
        return $dataCliente->points;
    }

    private function calcularMontoTotal($lista){
        $montoTotal = 0;
        $costoEnvio = 100;
        foreach($lista as $l){
            $montoTotal += $l->monto;
        }
        return $montoTotal;
    }

    public function calcularCostoEnvio(){

    }

    private function calcularMontoAporteTotal($lista){
        $montoAporteTotal = 0;

        foreach($lista as $l){
            $montoAporteTotal += $l->montoAporte;
        }

        return $montoAporteTotal;
    }

    public function repartidorData(Request $request){
       $dataCliente = DB::table('delivery_people')->where('id',$request->id_usuario)->first();
       return $mensaje=array('status_response' => "SUCCESS",'data' => $dataCliente);
    }

    public function finishOrder(Request $request){
        $listaProductos = array(json_decode($request->carrito));
        $lista = [];
        
        foreach($listaProductos as $a){
            foreach($a as $key => $b){
                $elemento = DB::table('products')->where('product_code',$b->codigo)->first();
                $elemento->product_quantity = $b->cantidad;
                $elemento->monto = $b->cantidad * $elemento->product_cost;
                $elemento->montoAporte = $b->cantidad * $elemento->product_com_profit;
                array_push($lista,$elemento);
            }
        }
        $this->asignarAporteComunitario($request->cliente,$this->calcularMontoAporteTotal($lista),$request->codigo);
        $this->asignarPuntosCliente($request->cliente,$request->puntos);
        DB::table('shoppings')->where('codigo',$request->codigo)->update(['status'=>'PF']);
        return response()->json(['status' => '202','mensaje' => 'Pedido finalizado correctamente!']);
    }

    public function asignarAporteComunitario($cliente,$monto,$codigoCompra){
        $afiliacion = \App\Affiliation::where('user_id',$cliente)->first();
        if($afiliacion){
            \App\Project::where('project_code',$afiliacion->project_code)->increment('project_collected_amount',$monto);
            $this->registroAporteProyectoComunitario($cliente,$afiliacion,$monto,'COMPRA',$codigoCompra);
        }
    }

    private function registroAporteProyectoComunitario($cliente,$afiliacion,$m,$type,$codigoCompra){
        error_log($m);
        \App\ProjectIncome::create([
            'project_code' => $afiliacion->project_code,
            'client_id' => $cliente,
            'shopping_code' => $codigoCompra,
            'income_type' => $type,
            'income' => $m
        ]);
    }

    public function asignarPuntosCliente($cliente,$puntos){
        DB::table('users')->where('id',$cliente)->increment('points',$puntos);
    }

    public function cancelAccount(Request $request){
        DB::table('users')->where('id',$request->client_id)->update(['client_status'=>$request->client_status]);
        return response()->json(['status' => '202','mensaje' => 'Cliente eliminado correctamente!']);

    }

    public function updateDevice(Request $request){
        DB::table('users')->where('id',$request->client_id)->update(['player_id'=>$request->client_device]);
        return response()->json(['status' => '202','mensaje' => 'Dispositivo actualizado correctamente!']);
    }

    public function updateDeliveryDevice(Request $request){
        DB::table('delivery_people')->where('id',$request->delivery_person_id)->update(['player_id'=>$request->delivery_person_device]);
        return response()->json(['status' => '202','mensaje' => 'Dispositivo actualizado correctamente!']);
    }

    public function getOrderDetail(Request $request){
        $order = DB::table('shoppings')->where('codigo',$request->order_code)->first();
        return $mensaje=array('status' => "202",'order_status' => $order->status);
    }
    
    public function appVersion(){
        return $mensaje=array('version' => "1.0.0");
    }
    
    public function updateClientPicture(Request $request){
        $rutaImagen = "";
        $image = base64_decode($request->user_image);
        if($image){
            $rutaImagen = $this->imageGenerator($request);
        }
        
        DB::table('users')->where('id',$request->client_id)->update(['picture'=>$rutaImagen]);
        
        $status="202";
        if($rutaImagen == ""){
            $status="500";
        }
        
        
        return response()->json(['status' => $status]);
        
    }

    public function communityOrganizationChart(Request $request){
        $organization_chart = \App\communityOrganizationChart::where('warehouse_code',$request->warehouse_code)->first();
        return $organization_chart;
    }

    public function showAdvertisements(){
        $advertisements = \App\Advertisement::where('status','A')->get();
        return $advertisements;
    }
    
    private function imageGenerator($request){
        $rutaImagen = "";
        $image = base64_decode($request->user_image);
        $im = imageCreateFromString($image);
        
        
        $ancho_original = imagesx($im);
        $alto_original = imagesy($im);

        $copia = imagecreatetruecolor(300,300);

        imagecopyresampled($copia, $im, 0,0,0,0,300,300,$ancho_original,$alto_original);
        
        $nombreImagen= md5(gmdate('Y-m-d h:i:s \G\M\T'));
        $rutaImagen = $nombreImagen.".png";

        imagepng($copia,'img/client_images_2304UIC/'.$rutaImagen, 0);
        
        return $rutaImagen;
    }

}
