<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Subcategory;
use Product;
use DB;

class ProductController extends Controller
{

    public function create(Request $request){
        $rutaImagen = "";

        $reglas = array(
            'product_code' => 'string|unique:products|max:80|min:1|required',
            'product_name' => 'string|max:80|min:1|required',
            'made_in' => 'string|required',
            'product_description' => 'string|required',
            'product_cost' => 'integer|required',
            'product_com_profit' => 'integer|required',
            'product_quantity' => 'integer|required',
            'subcategory_code' => 'string|exists:subcategories|required',
            'product_status' => 'required|min:1|max:3',
            'product_image' => 'image|mimes:jpeg,png,jpg|required',
            );

    	$validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }

        if($request->file('product_image')){
            $rutaImagen = $this->imageGenerator($request);
        }
        
    	 \App\Product::create([
            'product_code' => $request['product_code'],
            'product_name' => $request['product_name'],
            'made_in' => $request['made_in'],
            'product_description' => $request['product_description'],
            'product_cost' => $request['product_cost'],
            'product_com_profit' => $request['product_com_profit'],
            'product_quantity' => $request['product_quantity'],
            'is_offer' => false,
            'subcategory_code' => $request['subcategory_code'],
            'product_status' => $request['product_status'],
            'product_image' => $rutaImagen
            ]);

    	return redirect()->back()->with('success', 'El producto se ha creado exitosamente!');
    }

    public function update(Request $request){
        $rutaImagen = "";

        $this->validate($request, [
            'product_code' => 'string|max:80|min:1|required',
            'product_name' => 'string|max:80|min:1|required',
            'made_in' => 'string|required',
            'product_description' => 'string|required',
            'product_cost' => 'required',
            'product_com_profit' => 'required',
            'product_quantity' => 'integer|required',
            'subcategory_code' => 'string|exists:subcategories|required',
            'product_status' => 'required|min:1|max:3',
            'product_image' => 'image|mimes:jpeg,png,jpg',
        ]);

        if($request->file('product_image')){
            $rutaImagen = $this->imageGenerator($request);
        }

        if($rutaImagen != null && $rutaImagen != ""){
            DB::table('products')->where('product_code',$request->product_code)->update([
                'product_name'=>$request->product_name,
                'made_in' => $request->made_in,
                'product_description'=>$request->product_description,
                'product_cost'=>$request->product_cost,
                'product_com_profit' => $request->product_com_profit,
                'product_quantity'=>$request->product_quantity,
                'subcategory_code'=>$request->subcategory_code,
                'product_status'=>$request->product_status,
                'product_image'=>$rutaImagen
                ]);
        }else{
            DB::table('products')->where('product_code',$request->product_code)->update([
                'product_name'=>$request->product_name,
                'made_in' => $request->made_in,
                'product_description'=>$request->product_description,
                'product_cost'=>$request->product_cost,
                'product_com_profit' => $request->product_com_profit,
                'product_quantity'=>$request->product_quantity,
                'subcategory_code'=>$request->subcategory_code,
                'product_status'=>$request->product_status
                ]);
        }

        return redirect()->back()->with('success', 'El producto se ha modificado exitosamente!')->withInput();
    }

    public function showSubcategories(Request $request){
    	$reglas = array(
            'category_code' => 'string|exists:categories|required',
            );

    	$validacion = Validator::make($request->all(),$reglas);

        if($validacion->fails()){
            return response()->json(['error' => 'NOT FOUND'], 404);
        }

        $subcategories = DB::table('subcategories')->where('category_code', $request->category_code)->get();

        return $subcategories;
    }

    public function show(Request $request){
        $product_data = \App\Product::where('product_code',$request->search_product_code)->first();
        $categories = DB::table('categories')->select('category_code', 'category_name')->get();
        $subcategories = DB::table('subcategories')->select('subcategory_code','category_code','subcategory_name')->get();
        if($categories && $subcategories){
            foreach($categories as $category){
                foreach($subcategories as $subcategory){
                    if($subcategory->category_code == $category->category_code && $product_data->subcategory_code == $subcategory->subcategory_code){
                        $product_data->category_code = $category->category_code;
                    break;
                    }
                }
            }
        }
        return view('modify-product',compact('product_data','categories','subcategories'));
    }

    private function imageGenerator($request){
        $rutaImagen = "";
         if($request->file('product_image')->getClientOriginalExtension() == "jpeg" || 
            $request->file('product_image')->getClientOriginalExtension() == "jpg" || 
            $request->file('product_image')->getClientOriginalExtension() == "png" || 
            $request->file('product_image')->getClientOriginalExtension() == "JPG"){
                $nombreImagen= md5(gmdate('Y-m-d h:i:s \G\M\T'));
                $original = imagecreatefromjpeg($request->file('product_image')->getRealPath());
                $ancho_original = imagesx($original);
                $alto_original = imagesy($original);

                $copia = imagecreatetruecolor(300,300);

                imagecopyresampled($copia, $original, 0,0,0,0,300,300,$ancho_original,$alto_original);

                imagejpeg($copia,public_path('img/product_images')."/".$nombreImagen.".".$request->file('product_image')->getClientOriginalExtension(),100);
               // $request->file('imagen')->move(public_path('imagenes_promociones'),$nombreImagen.".".$request->file('imagen')->getClientOriginalExtension());
                $rutaImagen = $nombreImagen.".".$request->file('product_image')->getClientOriginalExtension();
             
                imagedestroy($original);
                imagedestroy($copia);
        }  

        return $rutaImagen;
    }

    public function productListCart($products){
        $listaProductos = array(json_decode($products));
        $lista = [];
        foreach($listaProductos as $a){
            foreach($a as $key => $b){
                $elemento = DB::table('products')->where('product_code',$b->codigo)->first();
                $elemento->product_quantity = $b->cantidad;
                array_push($lista,$elemento);
            }
        }
        return $lista;
    }

    
    public function productCart(Request $request){
        error_log($request->cart_products);
        $listaProductos = array(json_decode($request->cart_products));
        $lista = [];
        foreach($listaProductos as $a){
            foreach($a as $key => $b){
                $elemento = DB::table('products')->where('product_code',$b->codigo)->first();
                $elemento->product_quantity = $b->cantidad;
                array_push($lista,$elemento);
            }
        }
        return $lista;
    }

}
