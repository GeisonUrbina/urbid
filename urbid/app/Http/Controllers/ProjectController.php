<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Warehouse;

use Project;

use Affiliation;

use User;

use DeliveryPerson;

use DB;

use GuzzleHttp\Client;

class ProjectController extends Controller
{
    public function create(Request $request){
        $rutaImagen = "";

        $reglas = array(
            'project_code' => 'string|unique:projects|max:80|min:1|required',
            'project_name' => 'string|max:80|min:1|required',
            'project_description' => 'string|required',
            'project_cost' => 'integer|required',
            'warehouse_code' => 'string|exists:warehouses|required',
            'project_status' => 'required|min:1|max:3',
            'project_image' => 'image|mimes:jpeg,png,jpg|required',
        );
        
    	$validacion = Validator::make($request->all(),$reglas);
        
        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }
        
        
        if($request->file('project_image')){
            $rutaImagen = $this->imageGenerator($request);
        }

        error_log($rutaImagen);
    	 \App\Project::create([
            'project_code' => $request['project_code'],
            'project_name' => $request['project_name'],
            'project_description' => $request['project_description'],
            'project_cost' => $request['project_cost'],
            'project_collected_amount' => 0,
            'warehouse_code' => $request['warehouse_code'],
            'project_status' => $request['project_status'],
            'project_image' => $rutaImagen
            ]);
    	return redirect()->back()->with('success', 'El proyecto se ha creado exitosamente!');
    }

    public function update(Request $request){
        $rutaImagen = "";

        $this->validate($request, [
            'project_code' => 'string|max:80|min:1|required',
            'project_name' => 'string|max:80|min:1|required',
            'project_description' => 'string|required',
            'project_cost' => 'integer|required',
            'warehouse_code' => 'string|exists:warehouses|required',
            'project_status' => 'required|min:1|max:3',
            'project_image' => 'image|mimes:jpeg,png,jpg',
        ]);

        if($request->file('project_image')){
            $rutaImagen = $this->imageGenerator($request);
        }

        if($rutaImagen != null && $rutaImagen != ""){
            DB::table('projects')->where('project_code',$request->project_code)->update([
                'project_name'=>$request->project_name,
                'project_description'=>$request->project_description,
                'project_cost'=>$request->project_cost,
                'warehouse_code'=>$request->warehouse_code,
                'project_status'=>$request->project_status,
                'project_image'=>$rutaImagen
                ]);
        }else{
            DB::table('projects')->where('project_code',$request->project_code)->update([
                'project_name'=>$request->project_name,
                'project_description'=>$request->project_description,
                'project_cost'=>$request->project_cost,
                'warehouse_code'=>$request->warehouse_code,
                'project_status'=>$request->project_status
                ]);
        }

        return redirect()->back()->with('success', 'El proyecto se ha modificado exitosamente!')->withInput();
    }

    public function show(Request $request){
        $project_data = \App\Project::where('project_code',$request->search_project_code)->first();
        $warehouses = DB::table('warehouses')->select('warehouse_code','warehouse_name')->get();
        return view('modify-project',compact('project_data','warehouses'));
    }

    private function imageGenerator($request){
        $rutaImagen = "";
        
         if($request->file('project_image')->getClientOriginalExtension() == "jpeg" || 
            $request->file('project_image')->getClientOriginalExtension() == "jpg" || 
            $request->file('project_image')->getClientOriginalExtension() == "png" || 
            $request->file('project_image')->getClientOriginalExtension() == "JPG"){
                $nombreImagen= md5(gmdate('Y-m-d h:i:s \G\M\T'));
                $original = imagecreatefromjpeg($request->file('project_image')->getRealPath());
                $ancho_original = imagesx($original);
                $alto_original = imagesy($original);

                $copia = imagecreatetruecolor(200,200);

                imagecopyresampled($copia, $original, 0,0,0,0,200,200,$ancho_original,$alto_original);
                
                imagejpeg($copia,public_path('img/project_images')."/".$nombreImagen.".".$request->file('project_image')->getClientOriginalExtension(),100);
               // $request->file('imagen')->move(public_path('imagenes_promociones'),$nombreImagen.".".$request->file('imagen')->getClientOriginalExtension());
                $rutaImagen = $nombreImagen.".".$request->file('project_image')->getClientOriginalExtension();
             
                imagedestroy($original);
                imagedestroy($copia);
        }  

        return $rutaImagen;
    }
}
