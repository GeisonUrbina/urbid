<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Shopping;
use DB;

use App\Http\Controllers\NotificationController;

class ShoppingController extends Controller
{
    public function create(Request $request){

        $reglas = array(
            'cart_products' => 'string|required',
            'position_latitud' => 'string|required',
            'position_longitud' => 'string|required'
            );

    	$validacion = Validator::make($request->all(),$reglas);
        if($validacion->fails()){
            return redirect()->back()->withInput()->withErrors($validacion->errors());
        }

        $listaProductos = array(json_decode($request->cart_products));
        $lista = [];
        foreach($listaProductos as $a){
            foreach($a as $key => $b){
                $elemento = DB::table('products')->where('product_code',$b->codigo)->first();
                $elemento->product_quantity = $b->cantidad;
                $elemento->monto = $b->cantidad * $elemento->product_cost;
                array_push($lista,$elemento);
            }
        }
        

        $montoTotal = $this->calcularMontoTotal($lista);

        if($request['forma_pago'] == "P"){
            $this->modificarPuntos($montoTotal + $request['monto_envio'],$request['cliente']);
        }
    	 try{
            \App\Shopping::create([
            'codigo' => $this->obtenerCodigo(now()),
            'montoTotal' => $montoTotal,
            'montoPagar' => $request['monto_pago_ingresado'],
            'montoEnvio' => $request['monto_envio'],
            'forma_pago' => $request['forma_pago'],
            'status' => 'PC',
            'id_usuario' => $request['cliente'],
            'id_repartidor' => 1,
            'carrito' => $request['cart_products'],
            'latitud' => $request['position_latitud'],
            'longitud' => $request['position_longitud']            
            ]);
            
            $administradores = DB::table('users')->where('client_role','AD')->get();
            foreach($administradores as $administrador){
                $notification = new NotificationController();
                $mensaje = "Se ha realizado un pedido";
                $res = $notification->enviarMensaje($administrador->player_id,$mensaje,"CL");
            }

         }catch (\Exception $e){
             error_log($e);
         }
    	 $mensaje = '{codigo:202,mensaje:Se ha creado exitosamente}';
    	return response()->json(['mensaje'=> $mensaje]);
    }

    /**
    *   Método encargado de devolver el codigo de compra
    *   @Param Date $fechaActual
    *   return String formato: AñoMesSegundosMinutosDiaHora 
    **/
    private function modificarPuntos($monto,$cliente){
        DB::table('users')->where('id',$cliente)->decrement('points',$monto);
    }

    private function obtenerCodigo($fechaActual){
        return  $fechaActual->format('y').$fechaActual->format('m').$fechaActual->format('s').$fechaActual->format('i').$fechaActual->format('d').$fechaActual->format('H');
    }

    private function calcularMontoTotal($lista){
        $montoTotal = 0;
        foreach($lista as $l){
            $montoTotal += $l->monto;
        }
        return $montoTotal;
    }

    public function clientOrderList(Request $requestApp){
        $listaCompras;
        try{
            $listaCompras = DB::select("
            select 
            S.codigo,
            S.montoTotal,
            S.status,
            S.forma_pago,
            S.carrito,
            S.created_at,
            S.montoEnvio,
            D.delivery_person_name,
            D.delivery_person_lastName,
            D.delivery_person_phone
            FROM `shoppings` S, `delivery_people` D WHERE  S.id_usuario = ? and D.id = S.id_repartidor and (S.status='PC' OR S.status='PE') order by S.created_at DESC",[$requestApp->user_id]);
        
        }catch (\Exception $e) {
            error_log("Error al consultar la base de datos. Lista de compras del cliente");
        }
        
        return $listaCompras;
    }

    public function deliveryOrderList(Request $requestApp){
        $listaCompras;
        try{
            $listaCompras = DB::select("select 
            S.codigo,
            S.montoTotal,
            S.status,
            S.forma_pago,
            S.carrito,
            S.created_at,
            S.latitud,
            S.longitud,
            D.delivery_person_name,
            D.delivery_person_lastName,
            D.delivery_person_phone,
            U.id as 'client_id',
            U.client_name,
            U.client_lastName,
            U.client_phone
            FROM 
            `shoppings` S, 
            `delivery_people` D, 
            `users` U 
            WHERE  S.id_repartidor = ? and D.id = ? and U.id = S.id_usuario and S.status='PE' order by S.codigo DESC"
            ,[$requestApp->user_id,$requestApp->user_id]);
        
        }catch (\Exception $e) {
            error_log("Error al consultar la base de datos. Lista de compras del repartidor");
        }
        
        return $listaCompras;
    }

    public function cancelOrder(Request $request){
        $usuario = DB::table('users')->where('id',$request->id_cliente)->get();
        $notification = new NotificationController();
        $mensaje = "Su orden de compra ".$request->orderCode." ha sido cancelada";
        $res = $notification->enviarMensaje($usuario[0]->player_id,$mensaje,"CL");
        DB::table('shoppings')->where('codigo',$request->orderCode)->update(['status'=>'CC']);
        return response()->json(['status' => '202','mensaje' => 'Cancelado exitosamente!']);
    }

    public function notifyArrival(Request $request){
        $usuario = DB::table('users')->where('id',$request->id_cliente)->get();
        $notification = new NotificationController();
        $mensaje = "El repartidor ha llegado con su pedido";
        $res = $notification->enviarMensaje($usuario[0]->player_id,$mensaje,"CL");
        return response()->json(['status' => '202','mensaje' => 'Llegada al destino exitosa!']);
    }

}
