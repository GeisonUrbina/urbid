<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Middleware;
use DB;

use App\Http\Controllers\NotificationController;

class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ['except' => ['clientOrders','routingAdministrationView']]);
        $this->middleware('login', ['only' => ['routingAdministrationView']]);
       
    }

    public function routingAdministrationView(){

        return redirect()->to('/orders');
    }

    public function orders(){
        $deliveryPeople = DB::table('delivery_people')->where('delivery_person_status', 'A')->get();
        $listaCompras = DB::table('shoppings')->where(function ($request){
            $request->where('status','=','PC')->orWhere('status','=','PE');
        })->orderBy('created_at', 'DESC')->get();
        $lista = [];
        
        return view('orders',compact('deliveryPeople','listaCompras','lista'));
    }

    public function orderDetail(Request $request){
        
        $deliveryPerson = DB::table('delivery_people')->where('id', $request->repartidor)->first();
        $listaCompra = DB::table('shoppings')->where('codigo','=',$request->pedido)->first();
        $lista = [];
        array_push($lista,app('App\Http\Controllers\ProductController')->productListCart($listaCompra->carrito));
        return view('order-detail',compact('deliveryPerson','listaCompra','lista'));
    }

    public function clients(){
		$clients = DB::table('users')->where('client_status','!=','PA')->where('client_role','CL')->get();
    	return view('clients',compact('clients'));
	}

	public function createProduct(){
    	$categories = DB::table('categories')->where('category_status', 'A')->get();
    	return view('create-product',compact('categories'));
    }

    public function modifyProduct(){
        return view('modify-product');
    }

    public function createCategory(){
    	return view('create-category');
    }

    public function modifyCategory(){
    	return view('modify-category');
    }

    public function createSubcategory(){
        $categories = DB::table('categories')->where('category_status', 'A')->get();
        return view('create-subcategory',compact('categories'));
    }

    public function modifySubcategory(){
        return view('modify-subcategory');
    }

    public function createWarehouse(){
        $provinces = DB::table('provinces')->select('province_code','province_name')->get();
        return view('create-warehouse',compact('provinces'));
    }

    public function modifyWarehouse(){
        return view('modify-warehouse');
    }

    public function createProject(){
        $warehouses = DB::table('warehouses')->select('warehouse_code','warehouse_name')->get();
        return view('create-project',compact('warehouses'));
    }

    public function modifyProject(){
        return view('modify-project');
    }

    public function createDeliveryPerson(){
        return view('create-delivery-person');
    }

    public function modifyDeliveryPerson(){
        $deliveryPeople = DB::table('delivery_people')->get();
        return view('modify-delivery-person',compact('deliveryPeople'));
    }

    public function clientOrders(){

        $deliveryPeople = DB::table('delivery_people')->get();
        $listaCompras = DB::table('shoppings')->where('id_usuario','=',Auth::user()->id)->get();
        $lista = [];
        
        return view('client-orders',compact('deliveryPeople','listaCompras','lista'));
    }

    public function modifyOrderDelivery(Request $request){
        if($request->repartidor){
            $usuario = DB::table('delivery_people')->where('id',$request->repartidor)->get();
            DB::table('shoppings')->where('codigo',$request->orderCode)->update(['id_repartidor'=>$request->repartidor]);
            $notification = new NotificationController();
            $mensaje = "La orden de compra ".$request->orderCode." le ha sido asignada";
            $res = $notification->enviarMensaje($usuario[0]->player_id,$mensaje,"DE");
        }
        return redirect()->to('/orders');
    }

    public function modifyOrderStatus(Request $request){
        $registro =  DB::table('shoppings')->where('codigo',$request->orderCode)->get();
        $usuario = DB::table('users')->where('id',$registro[0]->id_usuario)->get();
        
        DB::table('shoppings')->where('codigo',$request->orderCode)->update(['status'=>$request->estado]);

        $notification = new NotificationController();
        if($request->estado === 'CC'){
            $mensaje = "Su orden de compra ".$request->orderCode." ha sido cancelada";
            $res = $notification->enviarMensaje($usuario[0]->player_id,$mensaje,"CL");
        }else if($request->estado === 'PE'){
            $mensaje = "Productos listos para entregarse";
            $res = $notification->enviarMensaje($usuario[0]->player_id,$mensaje,"CL");
        }
        
        return redirect()->to('/orders');
    }


    public function createCommunityOrganizationChart(){
        $warehouses = DB::table('warehouses')->where('warehouse_status','A')->get();
        //return view('modify-delivery-person',compact('deliveryPeople'));
        return view('create-community-organization-chart',compact('warehouses'));
    }


    public function modifyCommunityOrganizationChart(){
        return view('modify-community-organization-chart');
    }

    public function createAdvertisement(){
        $warehouses = DB::table('warehouses')->where('warehouse_status','A')->get();
        return view('create-advertisement',compact('warehouses'));
    }

    public function modifyAdvertisement(){
        $warehouses = DB::table('warehouses')->where('warehouse_status','A')->get();
        return view('modify-advertisement',compact('warehouses'));
    }
}
