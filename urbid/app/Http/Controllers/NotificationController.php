<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
class NotificationController extends Controller
{
    public function enviarMensaje($device,$message,$tipo){
		$idApp = "";
		$tipo === "CL"? $idApp="531998ec-85dc-4b20-bf47-df8de55a51c6":$idApp="d0021d32-28bc-4a57-8b89-bb4448e6f5ac";
		
        $content = array(
			"en" => $message
			);
		
		$fields = array(
			'app_id' => $idApp,
			'include_player_ids' => array($device),
			'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
}
