<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $fillable = [
        'code', 'name','priority','category','position','status','warehouse_code','image'
    ];
}
