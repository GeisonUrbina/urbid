<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'client_name','client_lastName','client_phone','picture','facebook_id','player_id','phone_code','warehouse_code','points','phone_validated','email','validation_method','password','client_role','client_status','allow_notifications'
    ];
}
