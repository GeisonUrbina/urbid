<?php

Route::get('/', function () {
    return view('index');
})->name('login');

Route::get('/preguntas-frecuentes', function () {
    return view('faq');
});
Route::get('/terminos-condiciones', function () {
    return view('terminos-condiciones');
});


Route::get('/login', function () {
    return view('login');
});


Route::get('/welcome', function () {
    return view('welcome');
});


/*
MAIN CONTROLLER
*/
Route::get('/orders','MainController@orders');
Route::get('/order-detail','MainController@orderDetail');
Route::get('/clients','MainController@clients');
Route::get('/product-create','MainController@createProduct');
Route::get('/product-modify','MainController@modifyProduct');
Route::get('/category-create','MainController@createCategory');
Route::get('/category-modify','MainController@modifyCategory');
Route::get('/subcategory-create','MainController@createSubcategory');
Route::get('/subcategory-modify','MainController@modifySubcategory');
Route::get('/warehouse-create','MainController@createWarehouse');
Route::get('/warehouse-modify','MainController@modifyWarehouse');
Route::get('/project-create','MainController@createProject');
Route::get('/project-modify','MainController@modifyProject');
Route::get('/delivery-person-create','MainController@createDeliveryPerson');
Route::get('/delivery-person-modify','MainController@modifyDeliveryPerson');
Route::get('/client-orders','MainController@clientOrders');
Route::get('/routing','MainController@routingAdministrationView');
Route::get('/community-organization-chart-create','MainController@createCommunityOrganizationChart');
Route::get('/community-organization-chart-modify','MainController@modifyCommunityOrganizationChart');
Route::post('/modify-order-delivery','MainController@modifyOrderDelivery');
Route::post('/modify-order-status','MainController@modifyOrderStatus');
Route::get('/advertisement-create','MainController@createAdvertisement');
Route::get('/advertisement-modify','MainController@modifyAdvertisement');


/*******
USER CONTROLLER
********/

Route::get('update-client-status/{client_email}/{client_status}','UserController@update');
Route::post('/login-client-web','UserController@loginWeb');
Route::get('/logout-client-web','UserController@logoutWeb');

/*******
CATEGORY CONTROLLER
********/

Route::post('new-category','CategoryController@create');
Route::get('search-category','CategoryController@show');
Route::post('update-category','CategoryController@update');

/*******
SUBCATEGORY CONTROLLER
********/

Route::post('new-subcategory','SubcategoryController@create');
Route::post('update-subcategory','SubcategoryController@update');
Route::get('search-subcategory','SubcategoryController@show');

/*******
PRODUCT CONTROLLER
********/

Route::post('new-product','ProductController@create');
Route::post('search-subcategories','ProductController@showSubcategories');
Route::get('search-product','ProductController@show');
Route::post('update-product','ProductController@update');
Route::post('/administration/cart/cart-products','ProductController@productCart');


/*******
WAREHOUSE CONTROLLER
********/

Route::post('new-warehouse','WarehouseController@create');
Route::post('update-warehouse','WarehouseController@update');
Route::post('search-cantons','WarehouseController@showCantons');
Route::post('search-districts','WarehouseController@showDistricts');
Route::get('search-warehouse','WarehouseController@show');
Route::get('warehouses','WarehouseController@showAll');

Route::post('new-community-organization-chart','WarehouseController@createCommunityOrganizationChart');
Route::post('update-community-organization-chart','WarehouseController@updateCommunityOrganizationChart');
Route::get('search_organization_chart_by_warehouse_code','WarehouseController@organizationChartByWarehouseCode');

/*******
PROJECT CONTROLLER
********/

Route::post('new-project','ProjectController@create');
Route::post('update-project','ProjectController@update');
Route::get('search-project','ProjectController@show');

/*******
DELIVERY PERSON CONTROLLER
********/

Route::post('new-delivery-person','DeliveryPersonController@create');
Route::get('search-delivery-person','DeliveryPersonController@show');
Route::post('update-delivery-person','DeliveryPersonController@update');

/*******
SHOPPING CONTROLLER
********/

Route::post('/administration/shopping/order/processing','ShoppingController@create');
Route::post('/administration/order/orderList','ShoppingController@clientOrderList');
Route::post('/administration/order/deliveryOrderList','ShoppingController@deliveryOrderList');
Route::post('/administration/order/cancel','ShoppingController@cancelOrder');
Route::post('/administration/order/notify/arrival','ShoppingController@notifyArrival');


/*******
ADVERTISEMENT CONTROLLER
********/

Route::post('/new-advertisement','AdvertisementController@create');
Route::get('/search-advertisement','AdvertisementController@show');
Route::post('/update-advertisement','AdvertisementController@update');

/*******
APP CONTROLLER
********/
Route::post('/app/order/detail','AppController@getOrderDetail');
Route::post('/app/validate/customer','AppController@validateCustomer');
Route::post('/app/validate/deliveryPerson','AppController@validateDeliveryPerson');
Route::post('/app/phoneCode','AppController@requestPhoneCode');
Route::post('/app/client/new-client-app','AppController@createApp');
Route::post('/app/datosCliente','AppController@clientData');
Route::post('/app/modificar/nombre','AppController@modifyName');
Route::post('/app/modificar/apellido','AppController@modifySurname');
Route::post('/app/modificar/telefono','AppController@modifyPhone');
Route::post('/app/modificar/correo','AppController@modifyEmail');
Route::post('/app/modificar/comunidad','AppController@modifyCommunity');
Route::get('/app/category/categories','AppController@showAllCategories');
Route::get('/app/subcategory/subcategories/{category_code}','AppController@showAllSubcategories');
Route::get('/app/product/products/{subcategory_code}','AppController@showAllProducts');
Route::get('/app/product/promotions','AppController@showAllPromotions');
Route::post('/app/cart/cart-products','AppController@productCart');
Route::post('/app/searchProduct','AppController@searchProducts');
Route::get('/app/project/projects','AppController@showAllProjects');
Route::post('/app/project/projectsByWarehouse','AppController@showProjectsByWarehouse');
Route::post('/app/project/projectIncomeDetail','AppController@showProjectIncomeDetail');
Route::post('/app/projects/isAffiliate','AppController@isAffiliated');
Route::post('/app/projects/affiliations','AppController@modifyAffiliations');
Route::post('/app/projects/desaffiliation','AppController@disAffiliate');
Route::post('/app/receipt/receipt-list','AppController@receiptList');
Route::post('/app/datosRepartidor','AppController@repartidorData');
Route::post('/app/finish/order','AppController@finishOrder');
Route::post('/app/cancel/account','AppController@cancelAccount');
Route::post('/app/update/device','AppController@updateDevice');
Route::post('/app/update/delivery/device','AppController@updateDeliveryDevice');
Route::post('/app/client/modify/image','AppController@updateClientPicture');
Route::post('app/updated/client','AppController@updatedClientInfo');
Route::get('app/version','AppController@appVersion');
Route::post('app/community/organizationChart','AppController@communityOrganizationChart');
Route::get('app/advertisements','AppController@showAdvertisements');